# Пакет Artifico-pages

Этот пакет предоставляет управление страницами сайта в Artifico2.

Содержит в себе:

* список страниц с поиском;
* форму для редактирования страниц;
* отображение страниц в клиентской части;

## Установка

Т.к. это закрытый пакет, и satis в данный момент не используется, необходимо добавить репозиторий в конец composer.json:
``` json
"repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:artifico2/artifico-pages.git"
        }
    ]
```

Вы можете добавить пакет к Laravel версии 5.3.X выполнив команду:
``` bash
composer require "nutnet/artifico-pages dev-master"
```

После того как пакет был установлен добавьте его сервис-провайдер в config/app.php:
```php
// config/app.php
'providers' => [
    ...
    Nutnet\Artifico2\Pages\ServiceProvider::class
```

Теперь необходимо перенести ресурсы(миграции, конфиг, js, css, img) пакета в Laravel:
``` bash
php artisan vendor:publish
```

Для поднятия необходимой структуры базы данных выполните:
``` bash
php artisan migrate
```

Для отображения страниц в клиентской части:

* добавьте роут:

```php
// pages
Route::group(['as' => 'pages.'], function () {
    Route::get('/{alias}', 'Nutnet\Artifico2\Pages\App\Http\Controllers\Pages\IndexController@page')
        ->name('page')
        ->where('alias', '.*');
});
```

* настройте путь к view, например:

```php
return [
    'page_template' => 'pages.page'
];
```

* добавьте view в проект