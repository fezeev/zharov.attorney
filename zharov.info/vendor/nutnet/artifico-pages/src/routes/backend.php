<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => config('artifico2.root_path'), 'middleware' => ['backend'], 'as' => 'admin.'], function () {

        Route::resource('pages', 'Nutnet\Artifico2\Pages\App\Http\Controllers\Pages\Pages');
    });
});
