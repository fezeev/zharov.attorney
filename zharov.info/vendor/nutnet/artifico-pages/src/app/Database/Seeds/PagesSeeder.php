<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 08.12.16
 */

namespace Nutnet\Artifico2\Pages\App\Database\Seeds;

use Nutnet\Artifico2\Pages\App\Models\Page;
use Illuminate\Database\Seeder;
use Nutnet\Artifico2\Pages\App\Services\PagesLocale;

/**
 * Class PagesSeeder
 * @package App\Database\Seeds
 */
class PagesSeeder extends Seeder
{
    /**
     * @var PagesLocale
     */
    private $lang;

    /**
     * PagesSeeder constructor.
     * @param PagesLocale $lang
     */
    public function __construct(PagesLocale $lang)
    {
        $this->lang = $lang;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 3; $i++) {
            $page = new Page();
            $page->alias = 'page_'.$i;
            foreach ($this->lang->listLocales() as $locale => $locName) {
                $page->translateOrNew($locale)->name = "Page($locale) #$i";
                $page->translateOrNew($locale)->content = "Page($locale) #$i description";
            }
            $page->active = Page::STATUS_ACTIVE;
            $page->save();
        }
    }
}