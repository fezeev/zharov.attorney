<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 13.12.16
 */

namespace Nutnet\Artifico2\Pages\App\Services;

use Illuminate\Support\Facades\Lang;

/**
 * Class PagesLocale
 * @package Nutnet\Artifico2\Pages\App\Services
 */
class PagesLocale
{
    /**
     * @return mixed
     */
    public function current()
    {
        return Lang::getLocale();
    }

    /**
     * @return array
     */
    public function listLocales()
    {
        return config('artifico2.locales', [$this->current() => $this->current()]);
    }
}