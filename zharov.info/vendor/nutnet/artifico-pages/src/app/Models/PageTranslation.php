<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 12.12.16
 */

namespace Nutnet\Artifico2\Pages\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * @package Nutnet\Artifico2\Pages\App\Models
 */
class PageTranslation extends Model
{
    protected $table = 'pages_trans';

    public $timestamps = false;
}