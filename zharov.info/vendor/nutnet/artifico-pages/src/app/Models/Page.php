<?php

namespace Nutnet\Artifico2\Pages\App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Page extends Model
{
    use Translatable;

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'content',
        'meta_name',
        'meta_description',
        'meta_keywords',
        'meta_other'
    ];

    public static $STATUSES = [
        self::STATUS_ACTIVE => 'Активна',
        self::STATUS_DISABLED => 'Отключена'
    ];

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatusName($status)
    {
        return self::$STATUSES[$status];
    }

    /**
     * @param Builder $builder
     * @param $alias
     * @return Builder
     */
    public function scopeAlias(Builder $builder, $alias)
    {
        return $builder->where('alias', '=', $alias);
    }

    /**
     * @param array $request
     */
    public function fillFromRequest(array $request)
    {
        $this->alias    = $request['alias'];
        $this->active   = $request['active'];

        $translations = [];
        foreach ($this->translatedAttributes as $attr) {
            if (!isset($request[$attr])) {
                continue;
            }

            foreach ($request[$attr] as $locale => $value) {
                if (!isset($translations[$locale])) {
                    $translations[$locale] = [];
                }

                $translations[$locale][$attr] = $value;
            }
        }

        $emptyLocales = array_map(function ($fields) {
            return count(array_filter($fields)) == 0;
        }, $translations);
        $emptyLocales = array_keys(array_filter($emptyLocales));

        if ($this->exists && !empty($emptyLocales)) {
            $this->deleteTranslations($emptyLocales);
        }

        foreach ($translations as $locale => $fields) {
            if (in_array($locale, $emptyLocales)) {
                continue;
            }

            $trans = $this->translateOrNew($locale);
            foreach ($fields as $field => $value) {
                $trans->$field = $value;
            }
        }
    }
}
