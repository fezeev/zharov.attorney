<?php

namespace Nutnet\Artifico2\Pages\App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Nutnet\Artifico2\App\Http\Controllers\Controller;

use Nutnet\Artifico2\Pages\App\Models\Page;
use Nutnet\Artifico2\Pages\App\Services\PagesLocale;

class Pages extends Controller
{
    /**
     * @var array
     */
    private $locales;

    /**
     * @var string
     */
    private $currentLocale;

    /**
     * Pages constructor.
     * @param PagesLocale $lang
     */
    public function __construct(PagesLocale $lang)
    {
        $this->currentLocale = $lang->current();
        $this->locales = $lang->listLocales();

        $this->middleware('permission:view pages')->only('index');
        $this->middleware('permission:create page')->only('create', 'store');
        $this->middleware('permission:edit page')->only('edit', 'update');
        $this->middleware('permission:delete page')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('artifico-pages::pages.index', [
            'pages' => Page::whereTranslationLike('name', '%' . $request->search . '%')
                ->orWhere(
                    'alias',
                    'like',
                    '%' . $request->search . '%'
                )
                ->paginate(25),
            'search' => $request->search
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artifico-pages::pages.create', ['locales' => $this->locales]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name.'.$this->currentLocale       => 'required',
            'name.*' => 'max:255',
            'alias'      => 'required|max:255|unique:pages,alias',
            'content.'.$this->currentLocale     => 'required',

        ], [
            'name.*.required'     => 'Название обязательное поле',
            'name.*.max'          => 'Максимальная длина названия :max символов',
            'alias.required'    => 'Псевдоним обязательное поле',
            'alias.max'         => 'Максимальная длина псевдонима :max символов',
            'alias.unique'      => 'Страница с таким псевдонимом уже есть',
            'content.*.required'  => 'Текст обязательное поле',
        ]);

        $page = new Page();
        $page->fillFromRequest($request->all());
        $page->save();

        $request->session()->flash(
            'success',
            sprintf('Страница успешно добавлена')
        );

        return redirect()->route('admin.pages.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('artifico-pages::pages.edit', ['page' => Page::find($id), 'locales' => $this->locales]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var Page $page */
        $page = Page::findOrFail($id);
        $this->validate($request, [
            'name.'.$this->currentLocale       => 'required',
            'name.*' => 'max:255',
            'alias'      => 'required|max:255|unique:pages,alias,' . $page->alias. ',alias',
            'content.'.$this->currentLocale     => 'required',

        ], [
            'name.*.required'     => 'Название обязательное поле',
            'name.*.max'          => 'Максимальная длина названия :max символов',
            'alias.required'    => 'Псевдоним обязательное поле',
            'alias.max'         => 'Максимальная длина псевдонима :max символов',
            'alias.unique'      => 'Страница с таким псевдонимом уже есть',
            'content.*.required'  => 'Текст обязательное поле',
        ]);

        $page->fillFromRequest($request->all());
        $page->save();

        $request->session()->flash(
            'success',
            sprintf('Страница успешно обновлена')
        );

        return redirect()->route('admin.pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $page->delete();

        $request->session()->flash(
            'success',
            sprintf('Страница успешно удалена')
        );
    }
}
