<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 12.12.16
 */

namespace Nutnet\Artifico2\Pages\App\Http\Controllers\Pages;

use Nutnet\Artifico2\App\Http\Controllers\Controller;
use Nutnet\Artifico2\Pages\App\Models\Page;

class IndexController extends Controller
{
    /**
     * @var Page
     */
    private $model;

    /**
     * Pages constructor.
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->model = $page;
    }

    /**
     * Show page
     * @param $alias
     * @return mixed
     */
    public function page($alias)
    {
        /** @var Page $page */
        $page = $this->model->alias($alias)->firstOrFail();

        return view(config('artifico-pages.page_template'), [
            'page' => $page
        ]);
    }
}