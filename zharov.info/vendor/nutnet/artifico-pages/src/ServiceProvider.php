<?php

namespace Nutnet\Artifico2\Pages;

use Carbon\Carbon;
use Dimsav\Translatable\TranslatableServiceProvider;
use Illuminate\Support\ServiceProvider as SP;
use Illuminate\Routing\Router;

class ServiceProvider extends SP
{
    /**
     * Загрузка сервисов пакета. Вызывается после регистрации
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        // Публикуем миграции
        $this->publishMigrations();

        // Загружаем вью
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'artifico-pages');

        $this->publishes([
            __DIR__ . '/config/artifico-pages.php' => config_path('artifico-pages.php')
        ]);

        // Подключаем роуты
        require __DIR__ . '/routes/backend.php';
    }

    /**
     * Регистрация сервисов пакета
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(TranslatableServiceProvider::class);
    }

    private function publishMigrations()
    {
        $databasePath = __DIR__ . '/database/migrations';
        $timestamp = Carbon::now();
        if (!class_exists('CreatePagesTable')) {
            $this->publishes([
                $databasePath . '/create_pages_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_create_pages_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertPagesModuleMenuTable')) {
            $this->publishes([
                $databasePath . '/insert_pages_module_menu_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_pages_module_menu_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertDefaultPagesPermissions')) {
            $this->publishes([
                $databasePath . '/insert_default_pages_permissions.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_default_pages_permissions.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('AddMetaAndMakeTranslatable')) {
            $this->publishes([
                $databasePath . '/add_meta_and_make_translatable.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_add_meta_and_make_translatable.php'
                ),
            ], 'migrations');
        }
    }
}
