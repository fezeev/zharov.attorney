<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 08.12.16
 */
return [
    'page_template' => 'artifico-pages::pages.page',
    'wysiwyg' => true,
];