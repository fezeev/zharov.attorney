# Changelog

All Notable changes to `Nutnet\Artifico2` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## NEXT - YYYY-MM-DD

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

## 2016-12-12
- Добавлено автоматическое заполнение alias
- Добавлена поддержка ajax запросов (x-csrf-token)

## 2016-12-18

### Added
- Добавлен виджет загрузки (pace.js)

## 2016-12-08

### Added
- Добавлена поддержка bower
- Добавлен плагин для сортировки 

## 2016-11-24

### Added
- Добавили фаил CHANGELOG.md для логирования изменений
