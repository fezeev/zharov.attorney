# Основной пакет Artifico2

Этот пакет предоставляет базу панели управления сайтом, основанной на PHP-фрейморвке [Laravel 5.3](http://laravel.com/docs/5.3) и Bootstrap-фреймворке [AdminLTE](https://almsaeedstudio.com/themes/AdminLTE/index2.html).

Содержит в себе:

* главную страницу со статистикой из Google Analytics
* layout панели управления;
* систему разделения прав;
* интерфейс для работы с настройками.

## Установка

Вы можете добавить пакет к Laravel версии 5.3.X добавив в раздел "require" composer.json:
``` json
"nutnet/artifico2": "dev-master"
```
Т.к. это закрытый пакет, и satis в данный момент не используется, необходимо добавить репозиторий в конец composer.json:
``` json
"repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:artifico2/artifico2.git"
        }
    ]
```
После этих действий выполните:
``` bash
composer install
```
Во время выполнения этой команды вас попросят указать  ваш логин и пароль от gitlab.com.

После того как пакет был установлен добавьте его сервис-провайдер в config/app.php:
```php
// config/app.php
'providers' => [
    ...
    Nutnet\Artifico2\ServiceProvider::class,
];
```

Для манипуляций с настройками приложения используется модуль Settings, для удобства его использования зарегистрируйте алиас фасада в `config/app.php`:
```php
    'aliases' => [
        ...
        'AppSettings' => Nutnet\Artifico2\App\Facades\AppSettings::class,
        ...
```

Теперь необходимо перенести ресурсы(миграции, конфиг, js, css, img) пакета в Laravel:
``` bash
php artisan vendor:publish
```
Добавьте следующие трейты к модели User:
``` php
use Spatie\Permission\Traits\HasRoles;
use EloquentFilter\Filterable;

class User extends Authenticatable
{
    use HasRoles, Filterable;

    // ...
}
```

Для поднятия необходимой структуры базы данных выполните:
``` bash
php artisan migrate
```

Для того, чтоб была возможность заполнить базу данных тестовыми значениями, без копирования боевой базы пропишите следующую строку:
```php
// database/seeds
public function run()
{
    // $this->call(UsersTableSeeder::class);
     $ $this->call(\Nutnet\Artifico2\Database\Seeds\AdministratorSeeder::class);
}
```

Для заполнения базы данных тестовыми значениями выполните:
``` bash
php artisan db:seed 
```

Для вывода данных из Google-Analytics [добавьте](https://github.com/spatie/laravel-analytics/blob/master/README.md) сервис провайдер config/app.php, корректные данные в конфигурационный фаил config/laravel-analytics.php, и json фаил для доступа к статистике.

## Сборка ресурсов

Для минификации и сборки ресурсов (js и css) используется [Elixir](https://laravel.com/docs/5.3/elixir).

Если вам нужно выполнить сборку ресурсов, то сначала установите npm:
```bash
npm install
```

Если вам нужно выполнить установку bower пакетов:
```bash
bower install
```

А потом выполните сборку:
```bash
gulp --production
```

## Использование

Панель управления располагается по адресу domain.com/backend
Данные для входа:
* Логин: admin@example.com
* Пароль: admin

## Мультиязычность
Административная часть пока идет только на русском, но в будущем, возможно, будет локализовываться. 
Некоторые пакеты (например, artifico-pages, artifico-navigation) позволяют управлять контентом на нескольких языках, по умолчанию язык один - русский.
Настроить список языков можно отредактировав конфигурацию artifico:
```php
// config/artifico2.php
    /*
    |--------------------------------------------------------------------------
    | Локали
    |--------------------------------------------------------------------------
    |
    | Укажите список языков, на которых доступен сайт
    */
    'locales' => [
        // iso2-код языка => Название
        'ru' => 'Русский'
    ]
```

## Дополнительные поля для пользователей

Работает с помощью `plank/laravel-metable`.

Как включить доп. поля для пользователей: 

1. Подключите трейты `Plank\Metable\Metable` и `Nutnet\Artifico2\App\Models\Traits` к модели пользователя (обычно `App\Models\User`).
2. Модель пользователя должна реализовывать интерфейс `Nutnet\Artifico2\App\Contracts\UserWithParameters`
3. Реализуйте методы, которые требует этот интерфейс.

Пример настроенной модели:

```php
class User extends Authenticatable implements UserWithParametersContract
{
    use Notifiable, HasRoles, Filterable, Metable, UserWithParameters;
    
    // ... some code
    
    /**
     * @return array
     */
    public function paramsDefinition(): array
    {
        return [
            'vk' => [
                'name' => 'Ссылка VK', // название, которое будет отображаться на форме редактирования
                'validation' => 'required|url', // правила валидации поля
                'validation_messages' => [
                    'required' => 'Ссылка на VK обязательна',
                    'url' => 'Поле должно содержать ссылку',
                ], // сообщения валидации
            ],
            'fb' => [
                'name' => 'Ссылка FB',
                'widget' => self::PARAM_LONG_TEXT, // по умолчанию self::PARAM_STRING - инпут, self::PARAM_LONG_TEXT - textarea
                'validation' => [
                    'create' => 'url',
                    'update' => 'url|max:255',
                ], // разные правила валидации для создания/обновления
                'validation_messages' => [
                    'url' => 'Поле должно содержать ссылку',
                ],
            ],
        ];
    }
    
}
```

## История изменений

*

## Автотесты

*
