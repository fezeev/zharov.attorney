const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // Копируем зависимости в директорию пакета
    mix.copy(//Bootstrap
        'node_modules/admin-lte/bootstrap/css/bootstrap.min.css',
        'src/resources/assets/css'
    ).copy(//AdminLTE
        'node_modules/admin-lte/dist/css/AdminLTE.min.css',
        'src/resources/assets/css'
    ).copy(//Adminlte-blue-skin
        'node_modules/admin-lte/dist/css/skins/skin-blue.min.css',
        'src/resources/assets/css'
    ).copy(//Admin-lte-plugins
        'node_modules/admin-lte/plugins',
        'src/resources/assets/plugins'
    ).copy(//Bootstrap-fonts
        'node_modules/admin-lte/bootstrap/fonts',
        'src/resources/assets/fonts'
    ).copy(//Bootstrap-js
        'node_modules/admin-lte/bootstrap/js/bootstrap.min.js',
        'src/resources/assets/js'
    ).copy(//AdminLTE-js
        'node_modules/admin-lte/dist/js/app.min.js',
        'src/resources/assets/js/AdminLTE.min.js'
    ).copy(//AdminLTE-img
        'node_modules/admin-lte/dist/img',
        'src/resources/assets/img'
    );

    // Компилируем sass пакета
    mix.sass(
        './src/resources/assets/sass/app.scss',
        'src/resources/assets/css'
    );

    // Слияние css файлов в один
    mix.styles([
        './src/resources/assets/css/bootstrap.min.css',
        './src/resources/assets/plugins/datatables/dataTables.bootstrap.css',
        './src/resources/assets/plugins/select2/select2.min.css',
        './src/resources/assets/plugins/pace/pace.min.css',
        './src/resources/assets/css/AdminLTE.min.css',
        './src/resources/assets/css/skin-blue.min.css',
        './src/resources/assets/css/app.css'
    ], './src/resources/assets/css/artifico2.css');

    mix.scripts([
        './src/resources/assets/plugins/jQuery/jquery-2.2.3.min.js',
        './src/resources/assets/js/dropzone.js',
        './src/resources/assets/js/bootstrap.min.js',
        './src/resources/assets/js/AdminLTE.min.js',
        './src/resources/assets/plugins/select2/select2.full.min.js',
        './src/resources/assets/plugins/chartjs/Chart.min.js',
        './src/resources/assets/plugins/bootstrap-html5sortable/jquery.sortable.min.js',
        './src/resources/assets/plugins/pace/pace.min.js',
        './src/resources/assets/js/app.js',
        './src/resources/assets/js/admin_uploader.js'
    ],'./src/resources/assets/js/artifico2.js')
});
