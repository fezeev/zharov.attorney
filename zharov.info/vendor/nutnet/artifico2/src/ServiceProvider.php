<?php

namespace Nutnet\Artifico2;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider as SP;
use Illuminate\Routing\Router;
use Spatie\Analytics\AnalyticsFacade;

class ServiceProvider extends SP
{
    /**
     * Загрузка сервисов пакета. Вызывается после регистрации
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        // Публикуем ресурсы(img,js,css) админки
        $this->publishes([
            __DIR__ . '/resources/assets/css/artifico2.css' => public_path('/vendor/artifico2/artifico2.css'),
            __DIR__ . '/resources/assets/js/artifico2.js' => public_path('/vendor/artifico2/artifico2.js'),
            __DIR__ . '/resources/assets/img' => public_path('/vendor/artifico2/img'),
            __DIR__ . '/resources/assets/plugins' => public_path('/vendor/artifico2/plugins'),
            __DIR__ . '/resources/assets/fonts' => public_path('/vendor/fonts'),

        ], 'public');

        // Публикуем конфиг пакета
        $this->publishes([
            __DIR__ . '/config/artifico2.php' => config_path('artifico2.php'),
        ]);

        // Публикуем миграции
        /*$this->publishes([
            __DIR__ . '/database/migrations/' => database_path('migrations'),
        ], 'migrations');*/
        $this->publishMigrations();

        // Подключаем конфиги пакета
        $this->mergeConfigFrom(__DIR__ . '/config/artifico2.php', 'artifico2');

        // Подключаем middleware
        $router->middleware('backend', App\Http\Middleware\BackendAuthenticate::class);
        $router->middleware('backendGuest', App\Http\Middleware\RedirectIfAuthenticatedInBackend::class);
        $router->middleware('permission', App\Http\Middleware\PermissionMiddleware::class);

        // Загружаем вью
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'artifico2');

        // Подключаем роуты
        require __DIR__ . '/routes/backend.php';
    }

    /**
     * Регистрация сервисов пакета
     *
     * @return void
     */
    public function register()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        $this->app->register(\Spatie\Permission\PermissionServiceProvider::class);
        $this->app->register(\Creativeorange\Gravatar\GravatarServiceProvider::class);
        $this->app->register(\EloquentFilter\ServiceProvider::class);
        $this->app->register(\Plank\Metable\MetableServiceProvider::class);

        // Регистрирация фасада для настроек
        $this->app->bind('AppSettings', function(){
            return new App\Models\Setting;
        });

        $loader->alias('Gravatar', 'Creativeorange\Gravatar\Facades\Gravatar');
        $loader->alias('Analytics', AnalyticsFacade::class);
    }

    private function publishMigrations()
    {
        $databasePath = __DIR__ . '/database/migrations';
        $timestamp = Carbon::now();
        if (!class_exists('UpdateRolesTable')) {
            $this->publishes([
                $databasePath . '/update_roles_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_update_roles_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('UpdatePermissionsTable')) {
            $this->publishes([
                $databasePath . '/update_permissions_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_update_permissions_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertPermissionsTable')) {
            $this->publishes([
                $databasePath . '/insert_permissions_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_permissions_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertDefaultAdministratorAndRoles')) {
            $this->publishes([
                $databasePath . '/insert_default_administrator_and_roles.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_default_administrator_and_roles.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertDefaultSettingsPermissions')) {
            $this->publishes([
                $databasePath . '/insert_default_settings_permissions.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_default_settings_permissions.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('CreateSettingsTable')) {
            $this->publishes([
                $databasePath . '/create_settings_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_create_settings_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertDefaultSettings')) {
            $this->publishes([
                $databasePath . '/insert_default_settings.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_default_settings.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('CreateMenusTable')) {
            $this->publishes([
                $databasePath . '/create_menus_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_create_menus_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertIndexPermissions')) {
            $this->publishes([
                $databasePath . '/insert_index_permissions.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_index_permissions.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('MakeSettingsTranslatable')) {
            $this->publishes([
                $databasePath . '/make_settings_translatable.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_make_settings_translatable.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('CreateLivingRealties') && !class_exists('ExtraFieldsTable')) {
            $this->publishes([
                $databasePath . '/extra_fields_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_extra_fields_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('FixSettingsTranslatable')) {
            $this->publishes([
                $databasePath . '/fix_settings_translatable.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_fix_settings_translatable.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('ChangeUserPermissionNames')) {
            $this->publishes([
                $databasePath . '/change_user_permission_names.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_change_user_permission_names.php'
                ),
            ], 'migrations');
        }
    }
}
