<?php

namespace Nutnet\Artifico2\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @deprecated use Plank\Metable\Metable instead
 * Class ExtraField
 * @package App\Models
 */
class ExtraField extends Model
{
    public $table = 'extra_fields';

    protected $guarded = [];

    public $timestamps = false;
}
