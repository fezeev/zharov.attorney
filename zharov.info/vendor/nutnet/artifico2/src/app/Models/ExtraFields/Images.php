<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 07.12.16
 */

namespace Nutnet\Artifico2\App\Models\ExtraFields;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;
use Nutnet\Artifico2\App\Services\FileHelper;

class Images implements FieldInterface
{
    use ExtraFieldTrait;

    private $mimetypes = [
        'jpg'   => 'image/jpeg',
        'jpeg'  => 'image/jpeg',
        'bmp'   => 'image/x-ms-bmp',
        'gif'   => 'image/gif',
        'png'   => 'image/png',
        'tif'   => 'image/tiff',
        'tiff'  => 'image/tiff',
        'tga'   => 'image/x-targa',
        'psd'   => 'image/vnd.adobe.photoshop',
        'xbm'   => 'image/xbm',
        'pxm'   => 'image/pxm',
    ];

    /**
     * @inheritdoc
     */
    public function render(array $vars = [])
    {
        $tplVars = array_merge($this->options['template_vars'], $vars);
        $tplVars['bind'] = $this->prepareForUploader($this->options);

        return View::make($this->options['template'], $tplVars)->render();
    }

    /**
     * @param $data
     */
    public function beforeSave($data)
    {
        $oldValue = json_decode($this->field->value, true);
        $this->removeOldFiles($oldValue, $data);

        $this->field->value = json_encode($data);
        $this->saveUploadedImages($data);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        $value = json_decode($this->field->value, true);

        if (!is_array($value)) {
            return $value;
        }

        usort($value, function ($v1, $v2) {
            if ($v1['priority'] > $v2['priority']) {
                return 1;
            }

            return $v1['priority'] == $v2['priority'] ? 0 : -1;
        });

        return $value;
    }

    /**
     * Удалить файлы
     */
    public function beforeEntityDelete()
    {
        $this->removeOldFiles($this->getValue(), []);
    }

    /**
     * @param array $options
     * @return array
     */
    private function prepareForUploader(array $options)
    {
        $result = [];

        $photos = json_decode($this->field->value, true);

        if (empty($photos)) {
            return $result;
        }

        if (!is_array($photos)) {
            $photos = [$photos];
        }

        $useThumb = null;
        if (!empty($options['use_thumb'])) {
            $useThumb = $options['use_thumb'];
        }

        foreach ($photos as $photo) {
            $result[] = [
                'name' => $photo['value'],
                'url' => $this->assetUrl($photo['value']),
                'thumb_url' => $this->assetUrl($photo['value'], $useThumb),
                'size' => 0,
                'priority' => $photo['priority']
            ];
        }

        $prior = 1;
        foreach ($result as $key => $res) {
            $result[$key]['priority'] = $prior++;
        }

        return $result;
    }

    /**
     * @param array $images
     */
    public function saveUploadedImages(array $images)
    {
        $helper = $this->getFileHelper();

        foreach ($images as $image) {
            $tmpFile = $helper->assetPathLocal($image['value'], config('app.tmp_dir'), false);
            $assetPath = $helper->pathInStorage($image['value'], $this->options['upload_dir']);
            $targetDir = File::dirname($assetPath);

            /** @var Filesystem $storage */
            $storage = Storage::disk('uploads');

            if (!File::exists($targetDir)) {
                $storage->makeDirectory($targetDir, 493, true);
            }

            if (File::exists($tmpFile)) {
                if ($storage->exists($assetPath)) {
                    // примтивно сравниваем размер
                    if ($storage->size($assetPath) == File::size($tmpFile)) {
                        File::delete($tmpFile);
                        continue;
                    }
                }

                $res = fopen($tmpFile, 'r');
                try {
                    $this->putImage($storage, $assetPath, $res);
                    fclose($res);
                    $imageObj = Image::make($tmpFile);
                    foreach ($this->createThumbnails($assetPath, $imageObj) as $thumbFile => $thumb) {
                        $this->putImage($storage, $thumbFile, $thumb);
                        if (is_resource($thumb)) {
                            fclose($thumb);
                        }
                    }
                } catch (NotReadableException $e) {
                    continue;
                } finally {
                    if (is_resource($res)) {
                        fclose($res);
                    }
                    File::delete($tmpFile);
                }
            }
        }
    }

    public function assetUrl($image, $thumb = null, $default = null)
    {
        if (null !== $thumb && null !== $image) {
            $image = $this->getThumbPath($image, $thumb);
        }

        if (null === $image) {
            return $default;
        }

        return $this->getFileHelper()->assetUrl($image, $this->options['upload_dir']);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $this->options = array_merge(
            [
                'template' => 'artifico2::widgets.uploader',
                'template_vars' => [
                    'afterPreview' => 'artifico2::widgets.up_previews',
                    'identifier' => $this->field->identifier,
                    'origIdentifier' => $this->field->origin_identifier,
                ],
                'thumbs' => [],
                'upload_dir' => '.'
            ],
            $this->options ?? [],
            $options
        );
    }

    /**
     * @param $baseImagePath
     * @param \Intervention\Image\Image $image
     * @return array
     */
    private function createThumbnails($baseImagePath, \Intervention\Image\Image $image)
    {
        $thumbs = [];

        foreach ($this->options['thumbs'] as $thumbType => $opts) {
            $original = clone $image;
            $thumbs[$this->getThumbPath($baseImagePath, $thumbType)] = $original->fit(
                $opts['width'],
                $opts['height']
            )->stream()->detach();
        }

        return $thumbs;
    }

    /**
     * @param $baseImagePath
     * @param $thumbType
     * @return string
     */
    private function getThumbPath($baseImagePath, $thumbType)
    {
        return $this->getFileHelper()->getThumbPath($baseImagePath, $thumbType);
    }

    /**
     * @param $oldValue
     * @param $data
     */
    private function removeOldFiles($oldValue, $data)
    {
        if (!is_array($oldValue)) {
            return;
        }

        $removedFiles = array_diff(
            array_map(function ($v) {
                return $v['value'];
            }, $oldValue),
            array_map(function ($v) {
                return $v['value'];
            }, is_array($data) ? $data : [])
        );

        if (!empty($removedFiles)) {
            /** @var \Illuminate\Filesystem\FilesystemAdapter $storage */
            $storage = Storage::disk('uploads');

            $filesToRemove = [];
            foreach ($removedFiles as $original) {
                $filesToRemove = array_merge(
                    $filesToRemove,
                    $this->getAllFilesByOriginal($storage, $original)
                );
            }

            if (empty($filesToRemove)) {
                return;
            }

            $storage->delete($filesToRemove);
        }
    }

    /**
     * @param $storage
     * @param $original
     * @return array
     */
    private function getAllFilesByOriginal($storage, $original)
    {
        $files = [];
        $helper = $this->getFileHelper();

        $pattern = preg_replace('/\.'.File::extension($original).'$/', '', $original);
        $filesInDir = $storage->listContents(
            File::dirname($helper->pathInStorage($original, $this->options['upload_dir']))
        );

        $patternThumb = $pattern.'_';
        foreach ($filesInDir as $file) {
            if ($pattern == $file['filename']
                || strncmp($patternThumb, $file['filename'], strlen($patternThumb)) === 0
            ) {
                $files[] = $file['path'];
            }
        }

        return $files;
    }

    /**
     * @return FileHelper
     */
    private function getFileHelper()
    {
        return resolve('Nutnet\Artifico2\App\Services\FileHelper');
    }

    /**
     * @param \Illuminate\Contracts\Filesystem\Filesystem $disk
     * @param $path
     * @param $stream
     * @return bool
     */
    private function putImage(
        \Illuminate\Contracts\Filesystem\Filesystem $disk,
        $path,
        $stream
    )
    {
        /** @var \Illuminate\Filesystem\FilesystemAdapter $disk */
        return $disk->getDriver()->putStream($path, $stream, [
            'mimetype' => $this->getMimeByFileName($path)
        ]);
    }

    /**
     * @param $name
     * @return string
     */
    private function getMimeByFileName($name)
    {
        $ext = File::extension($name);

        if (isset($this->mimetypes[$ext])) {
            return $this->mimetypes[$ext];
        }
        reset($this->mimetypes);

        return current($this->mimetypes);
    }
}
