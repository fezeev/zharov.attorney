<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 07.12.16
 */

namespace Nutnet\Artifico2\App\Models\ExtraFields;

use Nutnet\Artifico2\App\Models\ExtraField;

/**
 * Class ExtraFieldTrait
 * @package App\Models\ExtraFields
 */
trait ExtraFieldTrait
{
    protected $field;

    protected $options;

    /**
     * ExtraFieldTrait constructor.
     * @param ExtraField $field
     * @param array $options
     */
    public function __construct(ExtraField $field, array $options = [])
    {
        $this->field = $field;
        $this->setOptions($options);
    }

    /**
     * @return mixed
     */
    public function getOriginId()
    {
        return $this->field->origin_identifier;
    }

    /**
     * @return ExtraField
     */
    public function getEntity(): ExtraField
    {
        return $this->field;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->field->value;
    }

    public function onDelete()
    {
        // ..
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $this->options = $options;
    }
}
