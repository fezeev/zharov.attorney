<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 07.12.16
 */

namespace Nutnet\Artifico2\App\Models\ExtraFields;

/**
 * Interface FieldInterface
 * @package App\Models\ExtraFields
 */
interface FieldInterface
{
    public function render(array $vars = []);

    public function beforeSave($data);

    public function getValue();
}
