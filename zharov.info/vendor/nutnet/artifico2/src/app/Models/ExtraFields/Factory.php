<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 07.12.16
 */

namespace Nutnet\Artifico2\App\Models\ExtraFields;

use Nutnet\Artifico2\App\Models\ExtraField;
use Illuminate\Database\Eloquent\Model;

class Factory
{
    const FIELD_IMAGES = 'images';

    private static $fields = [
        self::FIELD_IMAGES => Images::class
    ];

    /**
     * @param ExtraField $extraField
     * @param Model $entity
     * @return null
     */
    public static function create(ExtraField $extraField, Model $entity)
    {
        $id = $extraField->origin_identifier;

        if (!isset(self::$fields[$id])) {
            return null;
        }

        return new self::$fields[$id](
            $extraField,
            $entity->getEFieldSettings($extraField->identifier)
        );
    }
}
