<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 06.12.16
 */

namespace Nutnet\Artifico2\App\Models\ExtraFields;

use Nutnet\Artifico2\App\Models\ExtraField;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class WithExtraProperties
 * @package App\Models\Traits
 */
trait WithExtraProperties
{
    private $extras;

    private $inProcess = [];

    /**
     * @inheritdoc
     */
    protected static function boot()
    {
        static::deleting(function ($model) {
            // удаляем связанные поля
            foreach ($model->extrafields as $extrafield) {
                $model->extrafield($extrafield->identifier)->beforeEntityDelete();
                $extrafield->delete();
            }
        });

        $onChange = function ($model) {
            foreach ($model->getProcessExtraData() as $originId => $extra) {
                foreach ($extra as $identifier => $value) {
                    $model->extrafield($identifier, $originId)->beforeSave($value);
                }
            }

            $save = [];
            foreach ($model->allExtraFields() as $field) {
                $save[] = $field->getEntity();
            }
            $model->extrafields()->saveMany($save);
        };

        static::saved($onChange);

        parent::boot();
    }

    /**
     * @return HasMany
     */
    public function extrafields()
    {
        return $this
            ->hasMany(ExtraField::class, 'entity_id', 'id')
            ->where('entity', '=', static::class);
    }

    /**
     * @param $identifier
     * @param null $originIdentifier
     * @return \Nutnet\Artifico2\App\Models\ExtraFields\FieldInterface|null
     */
    public function extrafield($identifier, $originIdentifier = null)
    {
        $this->initExtras();

        if (!isset($this->extras[$identifier])) {
            return $this->extras[$identifier] = Factory::create(
                new ExtraField([
                    'identifier' => $identifier,
                    'origin_identifier' => $originIdentifier ?? $identifier,
                    'entity' => get_called_class(),
                    'entity_id' => $this->primaryKey
                ]),
                $this
            );
        }

        return $this->extras[$identifier];
    }

    /**
     * @return mixed
     */
    public function allExtraFields()
    {
        $this->initExtras();

        return $this->extras;
    }

    /**
     * @param array $data
     */
    public function setProcessExtraData(array $data)
    {
        $this->inProcess = $data;
    }

    /**
     * @return array
     */
    public function getProcessExtraData(): array
    {
        return $this->inProcess;
    }

    /**
     * @return array
     */
    public function extraFieldConfig()
    {
        return [];
    }

    /**
     * @param $id
     * @return array
     */
    public function getEFieldSettings($id)
    {
        return $this->extraFieldConfig()[$id] ?? [];
    }

    private function initExtras()
    {
        if (null !== $this->extras) {
            return;
        }

        $extras = [];

        /** @var ExtraField $field */
        foreach ($this->extrafields as $field) {
            $extras[$field->identifier] = Factory::create($field, $this);
        }

        $this->extras = $extras;
    }
}
