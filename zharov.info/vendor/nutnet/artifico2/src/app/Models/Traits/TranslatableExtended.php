<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 12.01.17
 */

namespace Nutnet\Artifico2\App\Models\Traits;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

trait TranslatableExtended
{
    use Translatable;

    /**
     * @param Builder $query
     * @param $orders
     * @param $baseLocale
     * @return Builder
     */
    public function scopeOrderByTranslation(Builder $query, $orders, $baseLocale)
    {
        $relation = $this->translations();
        $transTable = $relation->getRelated()->getTable();

        $query
            ->select([$this->getTable().'.*'])
            ->join(
                $transTable,
                $relation->getForeignKey(),
                '=',
                $relation->getQualifiedParentKeyName()
            )
            ->where($transTable.'.locale', '=', $baseLocale);

        if (!is_array($orders)) {
            $orders = [[$orders]];
        }

        $orders = array_map(function ($order) use ($transTable) {
            return [
                $transTable.'.'.$order[0],
                $order[1] ?? 'asc'
            ];
        }, $orders);

        foreach ($orders as $order) {
            $query->orderBy($order[0], $order[1]);
        }

        return $query;
    }

    /**
     * @param array $request
     */
    public function fillTranslationsFromRequest(array $request)
    {
        $translations = [];
        foreach ($this->translatedAttributes as $attr) {
            if (!isset($request[$attr])) {
                continue;
            }

            foreach ($request[$attr] as $locale => $value) {
                if (!isset($translations[$locale])) {
                    $translations[$locale] = [];
                }

                $translations[$locale][$attr] = $value;
            }
        }

        $emptyLocales = array_map(function ($fields) {
            return count(array_filter($fields)) == 0;
        }, $translations);
        $emptyLocales = array_keys(array_filter($emptyLocales));

        if ($this->exists && !empty($emptyLocales)) {
            $this->deleteTranslations($emptyLocales);
        }

        foreach ($translations as $locale => $fields) {
            if (in_array($locale, $emptyLocales)) {
                continue;
            }

            $trans = $this->translateOrNew($locale);
            foreach ($fields as $field => $value) {
                $trans->$field = $value;
            }
        }
    }
}
