<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 22.09.17
 */

namespace Nutnet\Artifico2\App\Models\Traits;

/**
 * Использовать совместно с Plank\Metable\Metable
 * Trait UserWithParameters
 * @package Nutnet\Artifico2\App\Models\Traits
 */
trait UserWithParameters
{
    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public function getParam($key, $default = null)
    {
        return $this->getMeta($key, $default);
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $params = array_map(function ($v) {
            if (empty($v)) {
                return null;
            }

            return $v;
        }, $params);

        $this->syncMeta($params);
    }

    /**
     * @param bool $isUpdate
     * @param string $containerField
     * @return array
     */
    public function getValidationRules($isUpdate = false, $containerField = 'parameters') : array
    {
        $rules = [];
        foreach ($this->paramsDefinition() as $name => $settings) {
            if (empty($settings['validation'])) {
                continue;
            }

            $ruleName = $containerField ? $containerField.'.'.$name : $name;

            $paramRules = $settings['validation'];
            if (is_string($paramRules) || $this->hasNumericKeys($paramRules)) {
                $rules[$ruleName] = $paramRules;
                continue;
            }

            $validationCase = $isUpdate ? 'update' : 'create';
            if (isset($paramRules[$validationCase])) {
                $rules[$ruleName] = $paramRules[$validationCase];
            }
        }

        return $rules;
    }

    /**
     * @param string $containerField
     * @return array
     */
    public function getValidationMessages($containerField = 'parameters') : array
    {
        $messages = [];
        foreach ($this->paramsDefinition() as $name => $settings) {
            if (empty($settings['validation_messages'])) {
                continue;
            }

            $messageNamespace = $containerField ? $containerField.'.'.$name : $name;
            $paramMessages = array_combine(
                array_map(function ($k) use ($messageNamespace) {
                    return $messageNamespace.'.'.$k;
                }, array_keys($settings['validation_messages'])),
                array_values($settings['validation_messages'])
            );

            $messages = array_merge($messages, $paramMessages);
        }

        return $messages;
    }

    /**
     * @param array $arr
     * @return bool
     */
    private function hasNumericKeys(array $arr)
    {
        return is_numeric(current(array_keys($arr)));
    }
}
