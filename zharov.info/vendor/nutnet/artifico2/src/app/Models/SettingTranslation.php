<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 12.12.16
 */

namespace Nutnet\Artifico2\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SettingTranslation
 * @package Nutnet\Artifico2\App\Models
 */
class SettingTranslation extends Model
{
    protected $table = 'settings_trans';

    public $timestamps = false;
}