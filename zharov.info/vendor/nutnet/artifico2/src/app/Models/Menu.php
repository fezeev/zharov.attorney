<?php

namespace Nutnet\Artifico2\App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'icon', 'href', 'params'];

    public function parent()
    {
        return $this->hasOne(Menu::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id');
    }

    /**
     * @param null $postBuilder
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function tree($postBuilder = null)
    {
        $query = static::with(implode('.', array_fill(0, 100, 'children')))
            ->where('parent_id', '=', null);

        if (is_callable($postBuilder)) {
            $query = call_user_func($postBuilder, $query);
        }

        return $query->get();
    }
}
