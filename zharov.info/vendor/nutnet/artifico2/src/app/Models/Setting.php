<?php

namespace Nutnet\Artifico2\App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Translatable;

    /**
     * @var bool
     */
    public $timestamps = false;

    public $incrementing = false;

    /**
     * @var array
     */
    public $translatedAttributes = [
        'value',
    ];

    protected $primaryKey = 'key';

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    protected $translationForeignKey = 'setting_key';

    /**
     * @param string $key
     * @param string $default
     * @param string $locale
     * @return string
     */
    public function get($key, $default = "default", $locale = null)
    {
        $result = \Cache::remember('Artifico2.AppSettings', 60*24*365, function () {
            return self::all()->groupBy('key');
        });

        if ($result->has($key))
            return $result
                ->get($key)
                ->first()
                ->translateOrDefault($locale)
                ->value;
        else
            return $default;
    }

    /**
     * @param array $request
     */
    public function fillFromRequest(array $request)
    {
        $this->key = $request['key'];
        $this->name = $request['name'];
        $this->group = $request['group'];

        $translations = [];
        foreach ($this->translatedAttributes as $attr) {
            if (!isset($request[$attr])) {
                continue;
            }

            foreach ($request[$attr] as $locale => $value) {
                if (!isset($translations[$locale])) {
                    $translations[$locale] = [];
                }

                $translations[$locale][$attr] = $value;
            }
        }

        $emptyLocales = array_map(function ($fields) {
            return count(array_filter($fields)) == 0;
        }, $translations);
        $emptyLocales = array_keys(array_filter($emptyLocales));

        if ($this->exists && !empty($emptyLocales)) {
            $this->deleteTranslations($emptyLocales);
        }

        foreach ($translations as $locale => $fields) {
            if (in_array($locale, $emptyLocales)) {
                continue;
            }

            $trans = $this->translateOrNew($locale);
            foreach ($fields as $field => $value) {
                $trans->$field = $value;
            }
        }
    }
}
