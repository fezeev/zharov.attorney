<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 22.09.17
 */

namespace Nutnet\Artifico2\App\Contracts;

interface UserWithParameters
{
    const PARAM_LONG_TEXT = 1;
    const PARAM_STRING = 2;

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getParam($key, $default = null);

    /**
     * @param array $params
     * @return void
     */
    public function setParams(array $params);

    /**
     * @param bool $isUpdate
     * @return array
     */
    public function getValidationRules($isUpdate = false) : array;

    /**
     * @param string $containerField
     * @return array
     */
    public function getValidationMessages($containerField = 'parameters') : array;

    /**
     * Пример:
     * return [
     *  'vk' => [
     *      'name' => 'Ссылка VK',
     *      'validation' => 'required|url',
     *      'validation' => ['required', 'url'], // идентично предыдущему правилу
     *      'validation' => [
     *          'create' => 'url', // правило действует на создание пользователя
     *          'update' => 'url|max:255', // правило действует на обновление пользователя
     *      ],
     *      'validation_messages' => [
     *          'required' => 'Ссылка на VK обязательна',
     *          'url' => 'Поле должно содержать ссылку',
     *      ],
     *  ],
     * ];
     * @return array
     */
    public function paramsDefinition() : array;
}