<?php

namespace Nutnet\Artifico2\App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedInBackend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->hasPermissionTo('visit backend') || Auth::user()->hasPermissionTo('*')) {
                return redirect(config('artifico2.root_path'));
            }
        }

        return $next($request);
    }
}
