<?php

namespace Nutnet\Artifico2\App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $checkResult = $this->check($request, $permission);

        if (true !== $checkResult) {
            return $checkResult;
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @param $permission
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function check(Request $request, $permission)
    {
        if (!$request->user()->hasPermissionTo($permission) && !$request->user()->hasPermissionTo('*')) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Forbidden.', 403);
            }

            return response(view('artifico2::errors.403'), 403);
        }

        return true;
    }
}
