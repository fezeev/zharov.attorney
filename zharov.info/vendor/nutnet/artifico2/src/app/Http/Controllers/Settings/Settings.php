<?php

namespace Nutnet\Artifico2\App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Nutnet\Artifico2\App\Http\Controllers\Controller;

use Nutnet\Artifico2\App\Models\Setting;

class Settings extends Controller
{
    /**
     * @var array
     */
    private $locales;

    /**
     * @var string
     */
    private $currentLocale;

    /**
     * Settings constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:view settings')->only('index');
        $this->middleware('permission:create setting')->only('create', 'store');
        $this->middleware('permission:edit setting')->only('edit', 'update');
        $this->middleware('permission:delete setting')->only('destroy');

        $this->currentLocale = Lang::getLocale();
        $this->locales = config('artifico2.locales', [$this->currentLocale => $this->currentLocale]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artifico2::settings.index', ['settings' => Setting::all()->groupBy('group')]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artifico2::settings.create', [
            'locales' => $this->locales,
            'activeLocale' => $this->currentLocale,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key'       => 'required|max:255|unique:settings,key',
            'name'      => 'required|max:255',
            'group'     => 'required|max:255',
            'value'     => 'required',

        ], [
            'key.required'      => 'Ключ обязательное поле',
            'key.unique'        => 'Настройка с таким ключом уже есть',
            'key.max'           => 'Максимальная длина ключа :max символов',
            'name.required'     => 'Название обязательное поле',
            'name.max'          => 'Максимальная длина названия :max символов',
            'group.required'    => 'Группа обязательное поле',
            'group.max'         => 'Максимальная длина группы :max символов',
            'value.required'    => 'Значение обязательное поле',
        ]);

        $setting = new Setting;
        $setting->fillFromRequest($request->all());
        $setting->save();

        $request->session()->flash(
            'success',
            sprintf('Настройка успешно добавлена')
        );

        \Cache::forget('Artifico2.AppSettings');

        return redirect()->route('admin.settings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function edit($key)
    {
        return view('artifico2::settings.edit', [
            'setting' => Setting::find($key),
            'locales' => $this->locales,
            'activeLocale' => $this->currentLocale,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key)
    {
        $setting = Setting::findOrFail($key);
        $this->validate($request, [
            'key'       => 'required|max:255|unique:settings,key,' . $setting->key .',key',
            'name'      => 'required|max:255',
            'group'     => 'required|max:255',
            'value'     => 'required',

        ], [
            'key.required'      => 'Ключ обязательное поле',
            'key.unique'        => 'Настройка с таким ключом уже есть',
            'key.max'           => 'Максимальная длина ключа :max символов',
            'name.required'     => 'Название обязательное поле',
            'name.max'          => 'Максимальная длина названия :max символов',
            'group.required'    => 'Группа обязательное поле',
            'group.max'         => 'Максимальная длина группы :max символов',
            'value.required'    => 'Значение обязательное поле',
        ]);

        $setting->fillFromRequest($request->all());
        $setting->save();

        $request->session()->flash(
            'success',
            sprintf('Настройка успешно обновлена')
        );

        \Cache::forget('Artifico2.AppSettings');

        return redirect()->route('admin.settings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $key)
    {
        $setting = Setting::findOrFail($key);
        $setting->delete();

        \Cache::forget('Artifico2.AppSettings');

        $request->session()->flash(
            'success',
            sprintf('Настройка успешно удалена')
        );
    }
}
