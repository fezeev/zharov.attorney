<?php

namespace Nutnet\Artifico2\App\Http\Controllers\Administrators;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Nutnet\Artifico2\App\Contracts\UserWithParameters;
use Nutnet\Artifico2\App\Http\Controllers\Controller;
use Nutnet\Artifico2\App\ModelsFilters\UserFilter;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Users extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view administrators')->only('index');
        $this->middleware('permission:create administrator')->only('create', 'store');
        $this->middleware('permission:edit administrator')->only('edit', 'update');
        $this->middleware('permission:delete administrator')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = config('auth.providers.users.model')::filter($request->all(), UserFilter::class)
            ->with('roles')
            ->paginateFilter(25);
        $data = [
            'user_filter' => $request->all(),
            'users' => $users,
            'roles' => Role::all()
        ];
        return view('artifico2::administrators.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'roles'       => Role::all(),
            'permissions' => Permission::all()->groupBy('group'),
            'user' => resolve(config('auth.providers.users.model'))->newInstance()
        ];
        return view('artifico2::administrators.users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = resolve(config('auth.providers.users.model'));
        $rules = [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
        $messages = [
            'name.required'      => 'Имя обязательное поле',
            'name.max'           => 'Максимальная длина имени :max символов',
            'email.required'     => 'Эл. почта обязательное поле',
            'email.email'        => 'Укажите, пожалуйста, корректную эл. почту',
            'email.max'          => 'Максимальная длина эл. почты :max символов',
            'email.unique'       => 'Пользователь с такой эл. почтой уже существует',
            'password.required'  => 'Пароль обязательное поле',
            'password.min'       => 'Минимальная длина пароля :min символов',
            'password.confirmed' => 'Пароли не совпадают',
        ];

        if ($model instanceof UserWithParameters) {
            $rules = array_merge($rules, $model->getValidationRules());
            $messages = array_merge($messages, $model->getValidationMessages());
        }

        $this->validate($request, $rules, $messages);

        $user = config('auth.providers.users.model')::create([
            'name'     => $request['name'],
            'email'    => $request['email'],
            'password' => bcrypt($request['password']),
        ]);

        if ($user instanceof UserWithParameters) {
            $user->setParams($request->get('parameters'));
        }

        if (!empty($request['roles'])) {
            $user->roles()->attach($request['roles']);
        }

        if (!empty($request['permissions'])) {
            $user->permissions()->attach($request['permissions']);
        }

        $request->session()->flash(
            'success',
            sprintf('Пользователь успешно добавлен')
        );

        return redirect()->route('admin.administrators.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = config('auth.providers.users.model')::with('roles', 'permissions')->findOrFail($id);
        $data = [
            'user'        => $user,
            'roles'       => Role::all(),
            'permissions' => Permission::all()->groupBy('group'),
        ];
        return view('artifico2::administrators.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = config('auth.providers.users.model')::findOrFail($id);

        $rules = [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users,email,' . $user->id,
            'password' => 'min:6|confirmed',
        ];
        $messages = [
            'name.required'      => 'Имя обязательное поле',
            'name.max'           => 'Максимальная длина имени :max символов',
            'email.required'     => 'Эл. почта обязательное поле',
            'email.email'        => 'Укажите, пожалуйста, корректную эл. почту',
            'email.max'          => 'Максимальная длина эл. почты :max символов',
            'email.unique'       => 'Пользователь с такой эл. почтой уже существует',
            'password.required'  => 'Пароль обязательное поле',
            'password.min'       => 'Минимальная длина пароля :min символов',
            'password.confirmed' => 'Пароли не совпадают',
        ];

        if ($user instanceof UserWithParameters) {
            $rules = array_merge($rules, $user->getValidationRules(true));
            $messages = array_merge($messages, $user->getValidationMessages());
        }

        $this->validate($request, $rules, $messages);

        $newFields = [
            'name'  => $request['name'],
            'email' => $request['email'],
        ];

        if (!empty($request['password'])) {
            $newFields['password'] = bcrypt($request['password']);
        }

        $user->update($newFields);
        if ($user instanceof UserWithParameters) {
            $user->setParams($request->get('parameters'));
        }

        if (!empty($request['roles'])) {
            $user->roles()->sync($request['roles']);
        }

        $user->permissions()->sync($request['permissions'] ?? []);

        $request->session()->flash(
            'success',
            sprintf('Пользователь успешно обновлен')
        );

        return redirect()->route('admin.administrators.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = config('auth.providers.users.model')::findOrFail($id);
        if (Auth::user()->id != $user->id) {
            $user->delete();

            $request->session()->flash(
                'success',
                sprintf('Пользователь успешно удален')
            );
        } else {
            $request->session()->flash(
                'error',
                sprintf('Вы не можете удалить свой аккаунт')
            );
        }
    }
}
