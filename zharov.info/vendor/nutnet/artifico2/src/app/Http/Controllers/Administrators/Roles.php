<?php

namespace Nutnet\Artifico2\App\Http\Controllers\Administrators;

use Illuminate\Http\Request;

use Nutnet\Artifico2\App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Roles extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view roles')->only('index');
        $this->middleware('permission:create role')->only('create', 'store');
        $this->middleware('permission:edit role')->only('edit', 'update');
        $this->middleware('permission:delete role')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artifico2::administrators.roles.index', ['roles' => Role::paginate(25)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artifico2::administrators.roles.create', ['permissions' => Permission::all()->groupBy('group')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'human_name' => 'required|max:255|unique:roles',
            'name'       => 'required|max:255|unique:roles',
        ], [
            'human_name.required' => 'Название обязательное поле',
            'human_name.max'      => 'Максимальная длина названия :max символов',
            'human_name.unique'   => 'Группа с таким назавнием уже есть',
            'name.required'       => 'Алиас обязательное поле',
            'name.max'            => 'Максимальная длина алиаса :max символов',
            'name.unique'         => 'Группа с таким алиасом уже есть',
        ]);

        $role = Role::create([
            'name'        => $request['name'],
            'human_name'  => $request['human_name'],
            'description' => $request['description'],
        ]);

        if (!empty($request['permissions'])) {
            $role->permissions()->attach($request['permissions']);
        }

        $request->session()->flash(
            'success',
            sprintf('Группа успешно добавлена')
        );

        return redirect()->route('admin.usergroups.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'role'        => Role::with('permissions')->findOrFail($id),
            'permissions' => Permission::all()->groupBy('group'),
        ];
        return view('artifico2::administrators.roles.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->validate($request, [
            'human_name' => 'required|max:255|unique:roles,human_name,' . $role->id,
            'name'       => 'required|max:255|unique:roles,name,' . $role->id,
        ], [
            'human_name.required' => 'Название обязательное поле',
            'human_name.max'      => 'Максимальная длина названия :max символов',
            'human_name.unique'   => 'Группа с таким назавнием уже есть',
            'name.required'       => 'Алиас обязательное поле',
            'name.max'            => 'Максимальная длина алиаса :max символов',
            'name.unique'         => 'Группа с таким алиасом уже есть',
        ]);

        $role->update([
            'name'        => $request['name'],
            'human_name'  => $request['human_name'],
            'description' => $request['description'],
        ]);

        if (!empty($request['permissions'])) {
            $role->permissions()->sync($request['permissions']);
        }

        $request->session()->flash(
            'success',
            sprintf('Группа успешно обновлена')
        );

        return redirect()->route('admin.usergroups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = Role::findOrFail($id);
        $user->delete();

        $request->session()->flash(
            'success',
            sprintf('Группа успешно удалена')
        );
    }
}
