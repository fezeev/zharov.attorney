<?php
/**
 * @author Maksim Khodyrev <maximkou@gmail.com>
 * Date: 14.11.2016
 */

namespace Nutnet\Artifico2\App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response as ResponseFacade;
use Illuminate\Support\Facades\Validator;
use Nutnet\Artifico2\App\Services\FileHelper;

/**
 * Class UploadController
 * @package App\Http\Controllers
 */
class UploadController extends Controller
{
    private $rules = [
        'image' => 'image|max:6000|mimes:jpeg,png'
    ];

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * UploadController constructor.
     * @param FileHelper $fileHelper
     */
    public function __construct(FileHelper $fileHelper)
    {
        $this->fileHelper = $fileHelper;
    }

    /**
     * @param Request $request
     * @param $type
     * @return mixed
     */
    public function upload(Request $request, $type)
    {
        try {
            $validation = Validator::make($request->all(), [
                'file' => $this->rules($type)
            ]);
        } catch (\InvalidArgumentException $e) {
            return Response::make('Unsupported upload type', 400);
        }

        if ($validation->fails()) {
            return ResponseFacade::make($validation->errors()->first(), 400);
        }

        $file = $request->file('file');

        $filename = $this->fileHelper->makeFileName(
            $file->extension()
        );
        $success = $file->move(
            $this->fileHelper->uploadDirLocal('tmp'),
            $filename
        );

        return new JsonResponse([
            'success' => (bool)$success,
            'filename' => $filename
        ], $success ? 200 : 400);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function remove(Request $request)
    {
        $file = $request->get('file');

        if (!$request->isMethod(Request::METHOD_POST) || !$file) {
            return new Response(null, 400);
        }

        $res = false;

        $file = $this->fileHelper->uploadDirLocal('tmp').'/'.$file;
        if (File::exists($file)) {
            $res = File::delete($file);
        }

        return new JsonResponse(['success' => $res]);
    }

    /**
     * @param $type
     * @return mixed|null
     */
    private function rules($type)
    {
        if (!isset($this->rules[$type])) {
            throw new \InvalidArgumentException();
        }

        return $this->rules[$type];
    }
}
