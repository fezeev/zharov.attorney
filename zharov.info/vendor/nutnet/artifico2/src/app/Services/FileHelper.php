<?php
/**
 * @author Maksim Khodyrev <maximkou@gmail.com>
 * Date: 14.11.2016
 */

namespace Nutnet\Artifico2\App\Services;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileHelper
 * @package App\Helpers
 */
class FileHelper
{
    /**
     * @var Filesystem
     */
    private $disk;

    /**
     * FileHelper constructor.
     */
    public function __construct()
    {
        $this->disk = Storage::disk('uploads');
    }

    /**
     * @param $filename
     * @param string $basePath
     * @param int $nameLength
     * @return string
     */
    public function makeDirName($filename, $basePath = '', $nameLength = 2)
    {
        $dirParts = str_split($filename, $nameLength);

        if (count($dirParts) < 2) {
            throw new \InvalidArgumentException('Too big nameLength');
        }

        return sprintf('%s/%s', $basePath.$dirParts[0], $dirParts[1]);
    }

    /**
     * @param null $ext
     * @return string
     */
    public function makeFileName($ext = null)
    {
        if (!$ext) {
            $ext = '';
        } else {
            $ext = '.'.$ext;
        }

        return sha1(uniqid()).$ext;
    }

    /**
     * @param null $subDir
     * @return string
     */
    public function uploadDirLocal($subDir = null)
    {
        $dir = public_path(config('app.upload_dir'));

        if (null !== $subDir) {
            $dir .= '/'.$subDir;
        }

        return $dir;
    }

    /**
     * @param $file
     * @param $baseDir
     * @return string
     */
    public function assetUrl($file, $baseDir)
    {
        $filename = $baseDir.'/'.$this->makeDirName($file).'/'.$file;

        return $this->disk->url($filename);
    }

    /**
     * @param $file
     * @param $baseDir
     * @param bool $optimized
     * @return string
     */
    public function assetPathLocal($file, $baseDir, $optimized = true)
    {
        return
            public_path(config('app.upload_dir'))
            .'/'.$this->pathInStorage($file, $baseDir, $optimized);
    }

    /**
     * @param $file
     * @param $baseDir
     * @param bool $optimized
     * @return string
     */
    public function pathInStorage($file, $baseDir, $optimized = true)
    {
        return $baseDir
            .($optimized ? '/'.$this->makeDirName($file) : '')
            .'/'.$file;
    }

    /**
     * @param $baseImagePath
     * @param $thumbType
     * @return string
     */
    public function getThumbPath($baseImagePath, $thumbType)
    {
        $dirname = File::dirname($baseImagePath);
        return sprintf(
            '%s%s_%s.%s',
            $dirname != '.' ? $dirname.'/' : '',
            File::name($baseImagePath),
            $thumbType,
            File::extension($baseImagePath)
        );
    }
}
