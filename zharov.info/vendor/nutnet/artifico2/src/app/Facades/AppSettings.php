<?php
namespace Nutnet\Artifico2\App\Facades;

use Illuminate\Support\Facades\Facade;

class AppSettings extends Facade {

    /**
     * Получить зарегистрированное имя компонента.
     *
     * Для вызова AppSettings::get('key'); в контроллере нужно добавить namespace
     * use Nutnet\Artifico2\App\Facades\AppSettings;
     *
     * Если не добавлять namespace, то можно вызвать \AppSettings::get('key');
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'AppSettings'; }
}