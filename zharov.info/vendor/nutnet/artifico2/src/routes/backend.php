<?php

Route::group(['middleware' => ['web']], function () {
    Route::get(
        config('artifico2.root_path') . '/login',
        'Nutnet\Artifico2\App\Http\Controllers\Auth\LoginController@showLoginForm'
    )->middleware('backendGuest');

    Route::post(
        config('artifico2.root_path') . '/login',
        'Nutnet\Artifico2\App\Http\Controllers\Auth\LoginController@login'
    );

    Route::get(
        config('artifico2.root_path') . '/logout',
        'Nutnet\Artifico2\App\Http\Controllers\Auth\LoginController@logout'
    )->name('admin.backend.logout');

    Route::group(['prefix' => config('artifico2.root_path'), 'namespace' => 'Nutnet\Artifico2\App\Http\Controllers', 'middleware' => ['backend'], 'as' => 'admin.'], function () {

        Route::get('/', 'Index\Index@Index')->name('dashboard');

        Route::post('/upload/{type}', 'UploadController@upload')->name('upload');
        Route::post('/uploads/remove', 'UploadController@remove')->name('remove_upload');

        Route::resource('administrators', 'Administrators\Users');
        Route::resource('usergroups', 'Administrators\Roles');
        Route::resource('settings', 'Settings\Settings');
    });
});
