$(document).ready(function($) {

    // CSFR заголовок для ajax запросов
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // AJAX-LOADER
    $(document).ajaxStart(function() {
        Pace.start();
    });
    $(document).ajaxStop(function() {
        Pace.stop();
    });

    // Автоматическое заполнение ALIAS
    if ($('input#name').length > 0 && $('input#alias').length > 0 && $('input#name').val() == '' && $('input#alias').val() == '') {
        $('input#name').keyup(function(){
            var $translit = translit($(this).val().toLowerCase());
            $('input#alias').val($translit);
            return false;
        });
    }

    // кликабельный ряд в таблице
    $(".row-link").click(function() {
        window.document.location = $(this).data("href");
    });

    // модальное окно подтверждения удаления
    $(".destroy-model").on('click', function(e) {
        e.preventDefault();
        $('#destroy-model').modal()
    });

    $("#destroy-model-accept").on('click', function(e) {
        e.preventDefault();
        var url = $(this).data('href');
        var redirect = $(this).data('redirect');
        $.ajax({
            url: url,
            type: 'DELETE',
            data: {
                _token: $('input[name=_token]').val()
            },
            success: function () {
                $('#destroy-model').modal('hide');
                window.location.href = redirect;
            }
        });
    });

    // инициализация поиска
    if ($(".multiple-select").length) {
        $(".multiple-select").select2();
    }

    // графики на главной странице
    var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: false,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: false,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: true,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 3,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 10,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: false,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
    };

    if ($("#visitsPerDayChart").length) {
        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#visitsPerDayChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);

        //Create the line chart
        areaChart.Line(visitsPerDayData, areaChartOptions);
    }

    if ($("#visitsLast30DaysChart").length) {
        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#visitsLast30DaysChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);

        //Create the line chart
        areaChart.Line(visitsLast30DaysData, areaChartOptions);
    }

    if ($("#visitsLast12MonthsChart").length) {
        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#visitsLast12MonthsChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);

        //Create the line chart
        areaChart.Line(visitsLast12MonthsData, areaChartOptions);
    }

    // CKEditor
    var editorFields = $("textarea[data-ckeditor='true']");
    if (editorFields.length > 0) {
        editorFields.each(function (i, elem) {
            CKEDITOR.replace($(elem).attr('id'), $(elem).data('ckeditor-config'));
        });
    }
});

function translit(value) {
    // Символ, на который будут заменяться все спецсимволы
    var space = '-';
    // Берем значение из нужного поля и переводим в нижний регистр
    var text = value;

    // Массив для транслитерации
    var transl = {
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
        'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
        'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
        'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh','ъ': space, 'ы': 'y', 'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',
        ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
        '#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
        '(': space, ')': space,'-': space, '\=': space, '+': space, '[': space,
        ']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
        '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
        '?': space, '<': space, '>': space, '№':space
    }

    var result = '';
    var current_sim = '';

    for(i=0; i < text.length; i++) {
        // Если символ найден в массиве то меняем его
        if(transl[text[i]] != undefined) {
            if(current_sim != transl[text[i]] || current_sim != space){
                result += transl[text[i]];
                current_sim = transl[text[i]];
            }
        }
        // Если нет, то оставляем так как есть
        else {
            result += text[i];
            current_sim = text[i];
        }
    }

    result = $.trim(result);

    return result;

}