$(document).ready(function () {
    var Uploader = function () {

    };

    Uploader.prototype = {
        dom: {
            previewsAfter: '._dz-preview-after',
            clickable: '._uploader_select_files',
            previewsContainer: '._uploader_previews'
        },

        instances: [],

        init: function () {
            var self = this;

            Dropzone.autoDiscover = false;
            $("._uploader").each(function (i, el) {
                self.instances[$(el).attr('id')] = new Dropzone('#' + $(el).attr('id'), {
                    url: $(el).data('load-url'),
                    clickable: $(self.dom.clickable, $(el)).get(0),
                    previewsContainer: $(self.dom.previewsContainer, $(el)).get(0),
                    addRemoveLinks: true,
                    removedfile: self.onRemove,
                    success: function (file, response) {
                        file.serverName = response.filename;

                        self.addServiceFields.call(self, this, file, $(file.previewElement));
                        $(document).trigger('uploader.file_added');
                    },
                    sending: self.onSend,
                    init: function () {
                        var inst = this;

                        var mocks = $(this.element).data('bind');
                        for (var mk in mocks) {
                            this.options.addedfile.call(this, mocks[mk]);
                            this.options.thumbnail.call(this, mocks[mk], mocks[mk]['url']);
                            self.addServiceFields.call(self, inst, mocks[mk], $(mocks[mk].previewElement));
                        }

                        $(document).trigger('uploader.initialized');
                    },
                    previewTemplate: self.getPreviewTpl()
                });

                if ($(el).data('disabled')) {
                    self.instances[$(el).attr('id')].disable();
                }
            });
        },

        addServiceFields: function (instance, file, previewEl) {
            var self = this;

            var tpl = $(self.dom.previewsAfter, instance.element);
            if (!tpl.length) {
                return;
            }

            var time = Date.now();

            previewEl.append(
                self.prepareTpl(
                    tpl.html(),
                    {
                        loop: time,
                        value: file.serverName ? file.serverName : file.name,
                        is_tpl: '',
                        param_id: file.id ? file.id : '',
                        priority: file.priority ? file.priority : instance.previewsContainer.childElementCount
                    }
                )
            );
        },

        onRemove: function (file) {
            var _ref;
            if (file.previewElement) {
                if ((_ref = file.previewElement) != null) {
                    _ref.parentNode.removeChild(file.previewElement);
                }
            }

            var maxFiles = this._updateMaxFilesReachedClass();
            $(document).trigger('uploader.file_removed');

            if (!file.serverName) {
                return maxFiles;
            }

            $.post($(this.element).data('remove-url'), {
                file: file.serverName,
                '_token': $('[name=_token]').val()
            });

            return maxFiles;
        },

        prepareTpl: function (tpl, placeholders) {
            for (var k in placeholders) {
                tpl = tpl.replace(new RegExp('\%' + k + '\%', 'g'), placeholders[k]);
            }

            return tpl;
        },

        onSend: function(file, xhr, formData) {
            formData.append("_token", $('[name=_token]').val());
        },

        getPreviewTpl: function () {
            return '<div class="dz-preview dz-file-preview col-md-1">' +
            '<div class="dz-details">' +
            '<img data-dz-thumbnail class="img-thumbnail _tooltip"/>' +
            '</div>' +
            '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
            '<div class="dz-error-message"><span data-dz-errormessage></span></div>' +
            '</div>';
        }
    };

    new Uploader().init();
});