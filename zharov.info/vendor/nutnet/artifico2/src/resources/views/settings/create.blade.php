@extends('artifico2::layout')

@section('title', 'Добавить настройку')
@section('page_header', 'Добавить настройку')


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ config('artifico2.root_path') }}"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li><a href="{{ route('admin.settings.index') }}">Настройки</a></li>
    </ol>
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-ban"></i> Ошибка</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-default">

        <form method="post" action="{{ route('admin.settings.store') }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('key') ? 'has-error' : '' }}">
                            <label for="key">Ключ</label>
                            <input type="text" class="form-control" id="key" name="key"
                                   value="{{ old('key') }}">
                            @if ($errors->has('key'))
                                {!! $errors->first('key','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Название</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                {!! $errors->first('name','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                            <label for="group">Группа</label>
                            <input type="text" class="form-control" id="group" name="group" value="{{ old('group') }}">
                            @if ($errors->has('group'))
                                {!! $errors->first('group','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12">
                        @include('artifico2::settings.widgets.value-field')
                    </div>
                </div>
            </div>
            <div class="box-footer nav">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" style="min-width:100px;"
                           value="Добавить">
                    <a href="{{ route('admin.settings.index') }}" class="btn btn-default"
                           style="min-width:100px;">Отмена</a>
                </div>
            </div>
        </form>
    </div>
@endsection