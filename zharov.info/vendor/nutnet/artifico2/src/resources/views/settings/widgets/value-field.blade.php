<div class="nav-tabs-custom">
    <?php $uniq = 'tab-locales-name'; ?>
    @if (count($locales) > 1)
        <ul class="nav nav-tabs">
            @foreach ($locales as $code => $name)
                <li @if($code == $activeLocale)class="active"@endif>
                    <a href="#tab_{{ $uniq }}_{{ $code }}" data-toggle="tab">{{ $name }}</a>
                </li>
            @endforeach
        </ul>
    @endif

    <div class="tab-content">
        @foreach ($locales as $code => $name)
            <div class="tab-pane{{ $code == $activeLocale ? ' active': '' }}" id="tab_{{ $uniq }}_{{ $code }}">
                <div class="form-group {{ $errors->has("value.$code") ? 'has-error' : '' }}">
                    <label for="value_{{ $code }}">Значение</label>
                    <textarea name="value[{{ $code }}]" id="value_{{ $code }}" class="form-control"
                              style="resize: vertical">{{ old("value.$code", isset($item) ? $item->translateOrNew($code)->value : '') }}</textarea>
                    @if ($errors->has("value.$code"))
                        {!! $errors->first("value.$code",'<span class="help-block">:message</span>')  !!}
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>