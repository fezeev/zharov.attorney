@extends('artifico2::layout')

@section('title', 'Редактировать настройку')
@section('page_header', 'Редактировать настройку')


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ config('artifico2.root_path') }}"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li><a href="{{ route('admin.settings.index') }}">Настройки</a></li>
    </ol>
@endsection

@section('content')
    <div class="modal fade" id="destroy-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Подтвердите действие</h4>
                </div>
                <div class="modal-body">
                    Вы действительно хотите удалить настройку {{$setting->key}}?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary"
                            data-href="{{ route('admin.settings.destroy', ['key' => $setting->key]) }}"
                            data-redirect="{{ route('admin.settings.index') }}"
                            id="destroy-model-accept">Удалить</button>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-ban"></i> Ошибка</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-default">
        <form  method="post" action="{{ route('admin.settings.update', ['key' => $setting->key]) }}">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('key') ? 'has-error' : '' }}">
                            <label for="key">Ключ</label>
                            <input type="text" class="form-control" id="key" name="key"
                                   value="{{ old('key') ? : $setting->key }}">
                            @if ($errors->has('key'))
                                {!! $errors->first('key','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Название</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ? : $setting->name }}">
                            @if ($errors->has('name'))
                                {!! $errors->first('name','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                            <label for="group">Группа</label>
                            <input type="text" class="form-control" id="group" name="group" value="{{ old('group') ? : $setting->group }}">
                            @if ($errors->has('group'))
                                {!! $errors->first('group','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12">
                        @include('artifico2::settings.widgets.value-field', ['item' => $setting])
                    </div>
                </div>
            </div>
            <div class="box-footer nav">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" style="min-width:100px;"
                           value="Сохранить">
                    <a href="{{ route('admin.settings.index') }}" class="btn btn-default"
                       style="min-width:100px;">Отмена</a>
                    @if(Auth::user()->hasPermissionTo('delete setting') || Auth::user()->hasPermissionTo('*'))
                    <span class="destroy-model btn btn-default pull-right"
                          style="min-width:100px;">Удалить</span>@endif
                </div>
            </div>
        </form>
    </div>
@endsection