@extends('artifico2::layout')

@section('title', 'Группы и права')
@section('page_header', 'Группы и права')

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-xs-12">
                        @if(Auth::user()->hasPermissionTo('create role') || Auth::user()->hasPermissionTo('*'))
                            <a href="{{ route('admin.usergroups.create') }}" class="btn btn-primary margin pull-right">
                                <i class="fa fa-plus"></i> Добавить</a>@endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped table-hover dataTable"
                               role="grid"
                               aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th>Название</th>
                                <th>Описание</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($roles as $role)
                                <tr role="row" class='row-link'
                                    data-href='{{ route('admin.usergroups.edit', ['usergroup' => $role->id]) }}'>
                                    <td>{{ $role->human_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-default btn-xs"
                                               href="{{ route('admin.usergroups.edit', ['usergroup' => $role->id]) }}"><i
                                                        class="fa fa-edit"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @if ( $roles->perpage() < $roles->total() )
                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" role="status" aria-live="polite">Показаны записи
                            с {{($roles->currentpage()-1)*$roles->perpage()+1}}
                            по {{($roles->currentpage()-1) * $roles->perpage() + $roles->count()}}
                            из {{$roles->total()}}</div>
                    </div>
                    <div class="col-sm-7">
                        <div class="dataTables_paginate paging_simple_numbers">
                            {{ $roles->links() }}
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection