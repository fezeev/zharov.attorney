@extends('artifico2::layout')

@section('title', 'Добавить пользователя')
@section('page_header', 'Добавить пользователя')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ config('artifico2.root_path') }}"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li><a href="{{ route('admin.administrators.index') }}">Пользователи</a></li>
    </ol>
@endsection

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-ban"></i> Ошибка</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('admin.administrators.store') }}">
        {{ csrf_field() }}
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#main" data-toggle="tab">Основные</a></li>
                <li><a href="#permissions" data-toggle="tab">Права</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="main">

                    <div class="form-group">
                        <img class="profile-user-img img-responsive img-circle"
                             src="{{Gravatar::get('email@example.com')}}"
                             alt="User profile picture">
                        <p class="help-block text-center">Аватар настраивается в <a href="https://ru.gravatar.com/"
                                                                                    target="_blank">gravatar.com</a></p>
                    </div>

                    <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name">Имя</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            {!! $errors->first('name','<span class="help-block">:message</span>')  !!}
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">Эл. почта</label>
                        <input type="email" class="form-control" id="email" name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            {!! $errors->first('email','<span class="help-block">:message</span>')  !!}
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control" id="password" name="password">
                        @if ($errors->has('password'))
                            {!! $errors->first('password','<span class="help-block">:message</span>')  !!}
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">Подтвердите пароль</label>
                        <input type="password" class="form-control" name="password_confirmation" id="password-confirm">
                    </div>

                    <div class="form-group">
                        <label>Группы</label>
                        @foreach($roles as $role)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"
                                           name="roles[{{ $role->id }}]"
                                           value="{{ $role->id }}"
                                           @if(array_key_exists($role->id, old('roles', []))) checked @endif > {{ $role->human_name }}
                                </label>
                            </div>
                        @endforeach
                    </div>

                    @include('artifico2::administrators.users.widgets.parameters', ['user' => $user])
                </div>

                <div class="tab-pane" id="permissions">
                    <div class="box box-solid">
                        <div class="box-group" id="accordion">
                            @foreach($permissions as $groupName => $permissionGroup)
                                <div class="panel box box-default">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion"
                                               href="#{{ str_slug($groupName, '_') }}" class="collapsed"
                                               aria-expanded="false">
                                                {{ $groupName }}
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="{{ str_slug($groupName, '_') }}" class="panel-collapse collapse"
                                         aria-expanded="false">
                                        <div class="box-body">
                                            <div class="form-group">
                                                @foreach($permissionGroup as $permission)
                                                    <div class=" checkbox ">
                                                        <label>
                                                            <input type="checkbox"
                                                                   name="permissions[{{ $permission->id }}]"
                                                                   value="{{ $permission->id }}"
                                                                   @if(array_key_exists($permission->id, old('permissions', []))) checked @endif > {{ $permission->human_name }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" style="min-width:100px;"
                           value="Добавить">
                    <a href="{{ route('admin.administrators.index') }}" class="btn btn-default"
                       style="min-width:100px;">Отмена</a>
                </div>
            </div>
        </div>
    </form>
@endsection