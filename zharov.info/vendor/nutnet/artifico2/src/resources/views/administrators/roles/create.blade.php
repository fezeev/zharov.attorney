@extends('artifico2::layout')

@section('title', 'Добавить группу')
@section('page_header', 'Добавить группу')


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ config('artifico2.root_path') }}"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li><a href="{{ route('admin.usergroups.index') }}">Группы и права</a></li>
    </ol>
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-ban"></i> Ошибка</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-default">

        <form method="post" action="{{ route('admin.usergroups.store') }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('human_name') ? 'has-error' : '' }}">
                            <label for="human_name">Название</label>
                            <input type="text" class="form-control" id="human_name" name="human_name"
                                   value="{{ old('human_name') }}">
                            @if ($errors->has('human_name'))
                                {!! $errors->first('human_name','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Алиас</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                {!! $errors->first('name','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea name="description" id="description" class="form-control"
                                      style="resize: vertical">{{ old('description') }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="box-group" id="accordion">
                    @foreach($permissions as $groupName => $permissionGroup)
                        <div class="panel box box-default">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#{{ str_slug($groupName, '_') }}" class="collapsed" aria-expanded="false">
                                        {{ $groupName }}
                                    </a>
                                </h4>
                            </div>
                            <div id="{{ str_slug($groupName, '_') }}" class="panel-collapse collapse"
                                 aria-expanded="false">
                                <div class="box-body">
                                    <div class="form-group">
                                        @foreach($permissionGroup as $permission)
                                            <div class=" checkbox ">
                                                <label>
                                                    <input type="checkbox"
                                                           name="permissions[{{ $permission->id }}]"
                                                           value="{{ $permission->id }}"
                                                           @if(array_key_exists($permission->id, old('permissions', []))) checked @endif > {{ $permission->human_name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="box-footer nav">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" style="min-width:100px;"
                           value="Добавить">
                    <a href="{{ route('admin.usergroups.index') }}" class="btn btn-default"
                           style="min-width:100px;">Отмена</a>
                </div>
            </div>
        </form>
    </div>
@endsection