@extends('artifico2::layout')

@section('title', 'Редактировать группу')
@section('page_header', 'Редактировать группу')


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ config('artifico2.root_path') }}"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li><a href="{{ route('admin.usergroups.index') }}">Группы и права</a></li>
    </ol>
@endsection

@section('content')
    <div class="modal fade" id="destroy-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Подтвердите действие</h4>
                </div>
                <div class="modal-body">
                    Вы действительно хотите удалить группу {{$role->name}}?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary"
                            data-href="{{ route('admin.usergroups.destroy', ['usergroup' => $role->id]) }}"
                            data-redirect="{{ route('admin.usergroups.index') }}"
                            id="destroy-model-accept">Удалить</button>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-ban"></i> Ошибка</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-default">
        <form  method="post" action="{{ route('admin.usergroups.update', ['administrator' => $role->id]) }}">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('human_name') ? 'has-error' : '' }}">
                            <label for="human_name">Название</label>
                            <input type="text" class="form-control" id="human_name" name="human_name" value="{{ old('human_name') ? : $role->human_name }}">
                            @if ($errors->has('human_name'))
                                {!! $errors->first('human_name','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Алиас</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ? : $role->name }}">
                            @if ($errors->has('name'))
                                {!! $errors->first('name','<span class="help-block">:message</span>')  !!}
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea name="description" id="description" class="form-control"
                                      style="resize: vertical">{{ old('description') ? : $role->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="box-group" id="accordion">
                    @foreach($permissions as $groupName => $permissionGroup)
                        <div class="panel box box-default">
                            <div class="box-header">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion"
                                       href="#{{ str_slug($groupName, '_') }}" class="collapsed" aria-expanded="false">
                                        {{ $groupName }}
                                    </a>
                                </h4>
                            </div>
                            <div id="{{ str_slug($groupName, '_') }}" class="panel-collapse collapse"
                                 aria-expanded="false">
                                <div class="box-body">
                                    <div class="form-group">
                                        @foreach($permissionGroup as $permission)
                                            <div class=" checkbox ">
                                                <label>
                                                    <input type="checkbox"
                                                           name="permissions[{{ $permission->id }}]"
                                                           value="{{ $permission->id }}"
                                                           @if(array_key_exists($permission->id, old('permissions', [])) || $role->permissions->contains('id', $permission->id)) checked @endif > {{ $permission->human_name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="box-footer nav">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" style="min-width:100px;"
                           value="Сохранить">
                    <a href="{{ route('admin.usergroups.index') }}" class="btn btn-default"
                       style="min-width:100px;">Отмена</a>
                    @if(Auth::user()->hasPermissionTo('delete role') || Auth::user()->hasPermissionTo('*'))
                    <span class="destroy-model btn btn-default pull-right"
                          style="min-width:100px;">Удалить</span>@endif
                </div>
            </div>
        </form>
    </div>
@endsection