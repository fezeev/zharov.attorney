<?php
if (!($user instanceof \Nutnet\Artifico2\App\Contracts\UserWithParameters) || empty($params = $user->paramsDefinition())) {
    return;
}
?>
@foreach ($params as $param => $settings)
    <div class="form-group {{ $errors->has("parameters.$param") ? 'has-error' : '' }}">
        <label for="parameter_{{ $param }}">{{ $settings['name'] }}</label>
        @if (($settings['widget'] ?? \Nutnet\Artifico2\App\Contracts\UserWithParameters::PARAM_STRING) == \Nutnet\Artifico2\App\Contracts\UserWithParameters::PARAM_STRING)
            <input type="text" class="form-control" id="parameter_{{ $param }}" name="parameters[{{ $param }}]"
                   value="{{ old("parameters.$param", $user->getParam($param)) }}">
        @else
            <textarea class="form-control" id="parameter_{{ $param }}" name="parameters[{{ $param }}]">{{ old("parameters.$param", $user->getParam($param)) }}</textarea>
        @endif

        @if ($errors->has("parameters.$param"))
            {!! $errors->first("parameters.$param",'<span class="help-block">:message</span>')  !!}
        @endif
    </div>
@endforeach