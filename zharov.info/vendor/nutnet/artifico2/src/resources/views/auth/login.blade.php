<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Artifico2 - Система управления сайтом</title>
    {{--Tell the browser to be responsive to screen width--}}
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{--REQUIRED CSS--}}
    <link rel="stylesheet" href="{{ URL::asset('vendor/artifico2/artifico2.css') }}">
    {{--Font Awesome--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    {{--Ionicons--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    {{--iCheck--}}
    <link rel="stylesheet" href="{{ URL::asset('vendor/artifico2/plugins/iCheck/square/blue.css') }}">

    {{--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries--}}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ config('artifico2.root_path')  }}">{!! AppSettings::get('cms.sitename') !!}</a>
    </div>
    <div class="login-box-body">
        <form role="form" method="POST" action="{{ config('artifico2.root_path') . '/login' }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email" class="form-control" name="email"  placeholder="Эл.почта" value="{{ old('email') }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    {!! $errors->first('email','<span class="help-block">:message</span>')  !!}
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="Пароль">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    {!! $errors->first('password','<span class="help-block">:message</span>')  !!}
                @endif
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Запомнить
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                </div>
            </div>
        </form>

        {{--Авторизация через социальные сети
        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                Google+</a>
        </div>

        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>--}}

    </div>
</div>

{{--REQUIRED JS SCRIPTS--}}
<script src="{{ URL::asset('vendor/artifico2/artifico2.js') }}"></script>

{{--AdminLTE App--}}
<script src="{{ URL::asset('vendor/artifico2/plugins/iCheck/icheck.min.js') }}"></script>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>
</body>
</html>
