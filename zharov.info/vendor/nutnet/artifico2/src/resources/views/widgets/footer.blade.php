<footer class="main-footer">
    {{--To the right--}}
    <div class="pull-right hidden-xs">
        {{--Anything you want--}}
    </div>
    {{--Default to the left--}}
    <strong>&copy; 2016{{ date('Y') >2016 ? ' &mdash; ' . date('Y') : '' }} <a href="http://nutnet.ru/" target="_blank">Nutnet</a></strong>
</footer>