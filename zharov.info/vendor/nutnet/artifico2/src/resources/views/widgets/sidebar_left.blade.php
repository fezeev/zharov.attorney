<aside class="main-sidebar">

    {{--sidebar: style can be found in sidebar.less--}}
    <section class="sidebar">

        {{--Sidebar user panel (optional)--}}
        {{--<div class="user-panel">
            <div class="pull-left image">
                <img src="vendor/artifico2/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                --}}{{--Status--}}{{--
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>--}}

        {{--search form (Optional)--}}
        {{--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>--}}
        {{--/.search form --}}

        {{--Sidebar Menu--}}
        @php
            $menus = \Nutnet\Artifico2\App\Models\Menu::tree();
            $routeName =  \Illuminate\Support\Facades\Route::getFacadeRoot()->current()->getName();

            $checkActive = function ($menus, $routeName) {
                foreach ($menus as $menu) {
                    if ($menu->href == $routeName) return true;
                }
                return false;
            }
        @endphp
        <ul class="sidebar-menu">
            @foreach($menus as $menu)
                @if(!$menu['children']->isEmpty())
                    <li class="treeview {{ $checkActive($menu['children'], $routeName)? "active": "" }}">
                        <a href="{{ $menu->href }}">
                            <i class="{{ $menu->icon }}"></i> <span>{{ $menu->title }}</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            @foreach($menu['children'] as $child)
                                <li @if($routeName == $child->href)class="active"@endif><a
                                            href="@if(!empty($child->params)){{ route($child->href, json_decode($child->params, true)) }}@else{{ route($child->href) }}@endif">
                                        <i class="{{ $child->icon }}"></i>{{ $child->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li @if($routeName == $menu->href)class="active"@endif><a href="{{ route($menu->href) }}"><i class="{{ $menu->icon }}"></i>
                            <span>{{ $menu->title }}</span></a></li>
                @endif
            @endforeach
        </ul>
        {{--/.sidebar-menu--}}
    </section>
    {{--/.sidebar--}}
</aside>
