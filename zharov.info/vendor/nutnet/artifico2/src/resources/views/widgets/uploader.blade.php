<?php $disabled = !empty($disabled); ?>
<div class="panel panel-default">
    <div class="panel-body _uploader" {!! $disabled ? 'data-disabled="true" ' : '' !!}id="upload_{{ uniqid() }}"
         data-bind="{{ json_encode($bind) }}"
         data-load-url="{{ route('admin.upload', ['type' => 'image']) }}"
         data-remove-url="{{ route('admin.remove_upload') }}"
    >
        <div style="display: none;" class="_dz-preview-after">
            @if (isset($afterPreview))
                @include($afterPreview, [
                    'origIdentifier' => $origIdentifier,
                    'identifier' => $identifier
                ])
            @endif
        </div>

        @if (!$disabled)
        <p>Перетащите сюда файлы или кликните на кнопку для выбора файлов</p>
        @endif

        <div class="_uploader_previews" style="overflow: hidden; margin-bottom: 10px;"></div>

        @if (!$disabled)
        <div><a href="javascript:void(0);" class="btn btn-primary _uploader_select_files">Выбрать файлы</a></div>
        @endif
    </div>
</div>