<?php

namespace Nutnet\Artifico2\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AdministratorSeeder extends Seeder
{
    /**
     * Создание тестового пользователя для первоначального входа в админку.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'       => 'Agent Smith',
            'email'      => 'admin@example.com',
            'password'   => bcrypt('admin'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('user_has_permissions')->insert([
            'user_id'       => '1',
            'permission_id' => '1',
        ]);
    }
}
