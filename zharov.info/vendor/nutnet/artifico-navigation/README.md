# Пакет Artifico-navigation

Этот пакет предоставляет управление навигацией сайта в Artifico2.

Содержит в себе:

* список пунктов меню;
* форму для редактирования пунктов меню;

## Установка

Вы можете добавить пакет к Laravel версии 5.3.X добавив в раздел "require" composer.json:
``` json
"nutnet/artifico-navigation": "dev-master"
```

Т.к. это закрытый пакет, и satis в данный момент не используется, необходимо добавить репозиторий в конец composer.json:
``` json
"repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:artifico2/artifico-navigation.git"
        }
    ]
```

После этих действий выполните:
``` bash
composer install
```
Во время выполнения этой команды вас попросят указать  ваш логин и пароль от gitlab.com.

После того как пакет был установлен добавьте его сервис-провайдер в config/app.php:
```php
// config/app.php
'providers' => [
    ...
    Nutnet\Artifico2\Navigation\ServiceProvider::class,];
```

и, если необходимо, зарегистрируйте алиас для фасада-помощника:
```php
// config/app.php
'aliases' => [
   ...
   'Navigation' => \Nutnet\Artifico2\Navigation\App\Facades\Navigation::class,
```

Теперь необходимо перенести ресурсы(миграции, конфиг, js, css, img) пакета в Laravel:
``` bash
php artisan vendor:publish
```

Измените настройки пакета, отредактировав файл `config/artifico-navigation.php`:
```php
    // название роута, отвечающего за отображение статической страницы на сайте
    'page_route' => null, 
    // название параметра роута, содержащего ID страницы, по умолчанию id
    'page_route_id_param' => 'id', 
    // параметр модели, отвечающий за идентификацию страницы (например, id или alias)
    'page_route_id_value_field' => 'id',
    // создавать абсолютные ссылки, если false - относительные
    'page_route_is_absolute' => true
```

Для поднятия необходимой структуры базы данных выполните:
``` bash
php artisan migrate
```

## Рендеринг меню / ссылки
С помощью фасада Navigation вы можете выполнять вывод меню (или отдельного элемента) в шаблон.

#### Метод menu 
Используется для вывода меню. Принимает аргументы:
 - _$parentNav_, родительский элемент, принимает алиас либо сущность Item. Если равен NULL, родителем будет считаться корневой элемент.
 - _$options_, настройки, описание ниже.
 ```php
 [
     // вцелом меню
     'menu' => [
         'element' => 'ul',
         // html-аттрибуты элемента меню
         'html_options' => [],
         // метод для проверки активен ли элемент 
         'check_active' => [$this, 'isActive']
     ],
     // опции элементов меню
     'menu_item' => [
         'element' => 'li',
         // класс активного элемента
         'active_class' => 'active',
         // html-aттрибуты ссылки
         'link_options' => [],
         // html-aттрибуты пункта
         'html_options' => []
     ],
     
     // настройки для вложенных меню
     'child_menu' => /* аналогично menu */,
     'child_menu_item' => /* аналогично menu_item */,
 ]
 ```
 - _$maxDepth_, макс. аложенность, по умолчанию равна 0 (текущий уровень).
 
Вывести плоское меню:
```php
Navigation::menu('test_menu')
```

Вывести вложенное меню, но не более 3 уровней:
```php
Navigation::menu('test_menu', [], 2)
```

#### Метод link
Выведет ссылку на переданный элемент навигации:
 - _$item_, сущность типа Item
 - _$options_, добавочные html-aттрибуты ссылки
