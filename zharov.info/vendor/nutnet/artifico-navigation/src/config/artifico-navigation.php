<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 08.12.16
 */
return [
    'page_route' => null,
    'page_route_id_param' => 'id',
    'page_route_id_value_field' => 'id',
    'page_route_is_absolute' => true
];