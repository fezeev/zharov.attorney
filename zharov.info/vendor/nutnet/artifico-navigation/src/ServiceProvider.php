<?php

namespace Nutnet\Artifico2\Navigation;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider as SP;
use Illuminate\Routing\Router;
use Nutnet\Artifico2\Navigation\App\Models\Item;
use Nutnet\Artifico2\Navigation\App\Services\Navigation;

class ServiceProvider extends SP
{
    /**
     * Загрузка сервисов пакета. Вызывается после регистрации
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        // Публикуем ресурсы(img,js,css) админки
        $this->publishes([
            __DIR__ . '/resources/assets/js/index.js' => public_path('/vendor/artifico-navigation/index.js'),
            __DIR__ . '/resources/assets/js/edit.js' => public_path('/vendor/artifico-navigation/edit.js'),

        ], 'public');

        $this->publishes([
            __DIR__ . '/config/artifico-navigation.php' => config_path('artifico-navigation.php')
        ]);


        // Публикуем миграции
        $this->publishMigrations();

        // Загружаем вью
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'artifico-navigation');

        // Подключаем роуты
        require __DIR__ . '/routes/backend.php';
    }

    /**
     * Регистрация сервисов пакета
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('artifico-navigation', function ($app) {
            return new Navigation($app->make(Item::class));
        });
    }

    private function publishMigrations()
    {
        $databasePath = __DIR__ . '/database/migrations';
        $timestamp = Carbon::now();
        if (!class_exists('CreateNavigationTable')) {
            $this->publishes([
                $databasePath . '/create_navigation_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_create_navigation_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertNavigationMenuTable')) {
            $this->publishes([
                $databasePath . '/insert_navigation_menu_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_navigation_menu_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('InsertDefaultNavigationPermissions')) {
            $this->publishes([
                $databasePath . '/insert_default_navigation_permissions.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute()->format('Y_m_d_His') . '_insert_default_navigation_permissions.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('UpdateNavigationTable')) {
            $this->publishes([
                $databasePath . '/update_navigation_table.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute(1)->format('Y_m_d_His') . '_update_navigation_table.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('MakeNavigationTranslatable')) {
            $this->publishes([
                $databasePath . '/make_navigation_translatable.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute(1)->format('Y_m_d_His') . '_make_navigation_translatable.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('NavigationAddLinkAttributes')) {
            $this->publishes([
                $databasePath . '/navigation_add_link_attributes.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute(1)->format('Y_m_d_His') . '_navigation_add_link_attributes.php'
                ),
            ], 'migrations');
        }

        if (!class_exists('MakeAliasOptional')) {
            $this->publishes([
                $databasePath . '/make_alias_optional.php.stub' => database_path(
                    '/migrations/' . $timestamp->addMinute(1)->format('Y_m_d_His') . '_make_alias_optional.php'
                ),
            ], 'migrations');
        }
    }
}
