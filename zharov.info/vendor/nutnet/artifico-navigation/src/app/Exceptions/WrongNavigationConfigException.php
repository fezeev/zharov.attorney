<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 29.12.16
 */

namespace Nutnet\Artifico2\Navigation\App\Exceptions;


class WrongNavigationConfigException extends \Exception
{

}