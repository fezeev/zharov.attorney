<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 29.12.16
 */

namespace Nutnet\Artifico2\Navigation\App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Navigation
 * @package Nutnet\Artifico2\Navigation\App\Facades
 */
class Navigation extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'artifico-navigation';
    }
}