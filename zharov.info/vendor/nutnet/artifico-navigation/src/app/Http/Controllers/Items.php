<?php

namespace Nutnet\Artifico2\Navigation\App\Http\Controllers\Items;

use Illuminate\Http\Request;
use Nutnet\Artifico2\App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Nutnet\Artifico2\Navigation\App\Models\Item;
use Nutnet\Artifico2\Pages\App\Models\Page;

class Items extends Controller
{
    /**
     * @var array
     */
    private $locales;

    /**
     * @var string
     */
    private $currentLocale;

    /**
     * Items constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:view navitems')->only('index');
        $this->middleware('permission:create navitem')->only('create', 'store');
        $this->middleware('permission:edit navitem')->only('edit', 'update');
        $this->middleware('permission:delete navitem')->only('destroy');

        $this->currentLocale = Lang::getLocale();
        $this->locales = config('artifico2.locales', [$this->currentLocale => $this->currentLocale]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!isset($request['id']) || empty($request['id'])) {
            $items = Item::whereIsRoot()->defaultOrder()->get();
        } else {
            $items = Item::where('parent_id', $request['id'])->defaultOrder()->get();
        }

        return view('artifico-navigation::items.index', [
            'items' => $items,
            'id' => $request['id'] ? $request['id'] : null
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artifico-navigation::items.create', [
            'items' => Item::defaultOrder()->get()->toTree(),
            'parent_id' => \Request::input('parent_id'),
            'pages' => Page::all(),
            'locales' => $this->locales,
            'activeLocale' => $this->currentLocale,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name.'.$this->currentLocale => 'required',
            'name.*' => 'max:255',
            'alias'      => 'max:255|unique:navigation,alias',
            'url' => 'required_if:type,'.Item::TYPE_LINK,
            'page_id' => 'required_if:type,'.Item::TYPE_PAGE,
        ], [
            'name.'.$this->currentLocale.'.required'     => 'Название обязательное поле',
            'name.*.max'          => 'Максимальная длина названия :max символов',
            'alias.max'         => 'Максимальная длина псевдонима :max символов',
            'alias.unique'      => 'Пункт с таким псевдонимом уже есть',
            'url.required_if' => 'Адрес обязателен для типа "Ссылка"',
            'page_id.required_if' => 'Страница обязательное поле для типа "Страница"',
        ]);

        $item = new Item();
        $item->fillFromRequest($request->all());
        $item->save();

        $request->session()->flash(
            'success',
            sprintf('Пункт успешно добавлен')
        );

        return redirect()->route('admin.navigation.index', ['id' => $item->parent_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('artifico-navigation::items.edit', [
            'item' => Item::find($id),
            'items' => Item::defaultOrder()->get()->toTree(),
            'pages' => Page::all(),
            'locales' => $this->locales,
            'activeLocale' => $this->currentLocale,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);

        $this->validate($request, [
            'name.'.$this->currentLocale => 'required',
            'name.*' => 'max:255',
            'alias'      => 'max:255|unique:navigation,alias,' . $item->alias. ',alias',
            'url' => 'required_if:type,'.Item::TYPE_LINK,
            'page_id' => 'required_if:type,'.Item::TYPE_PAGE,
        ], [
            'name.'.$this->currentLocale.'.required'     => 'Название обязательное поле',
            'name.*.max'          => 'Максимальная длина названия :max символов',
            'alias.max'         => 'Максимальная длина псевдонима :max символов',
            'alias.unique'      => 'Пункт с таким псевдонимом уже есть',
            'url.required_if' => 'Адрес обязателен для типа "Ссылка"',
            'page_id.required_if' => 'Страница обязательное поле для типа "Страница"',
        ]);

        $item->fillFromRequest($request->all());
        $item->save();

        $request->session()->flash(
            'success',
            sprintf('Пункт успешно обновлен')
        );

        return redirect()->route('admin.navigation.index', ['id' => $item->parent_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $item->delete();

        $request->session()->flash(
            'success',
            sprintf('Пункт успешно удален')
        );
    }

    /**
     * Sort navigation items
     *
     * @param  \Illuminate\Http\Request $request
     * @return json
     */
    public function sort(Request $request)
    {
        $item = Item::findOrFail($request->current_id);

        if ($request->exists('next_id')) {
            $nextItem = Item::findOrFail($request->next_id);

            print_r($item);

            $item->insertBeforeNode($nextItem);

            print_r($item);

            $response = array(
                'status' => 'success1',
            );

            return \Response::json($response);

        } elseif ($request->exists('prev_id')) {
            $prevItem = Item::findOrFail($request->prev_id);
            $item->insertAfterNode($prevItem);

            $response = array(
                'status' => 'success2',
            );

            return \Response::json($response);

        }

        $response = array(
            'status' => 'success',
        );

        return \Response::json($response);
    }
}
