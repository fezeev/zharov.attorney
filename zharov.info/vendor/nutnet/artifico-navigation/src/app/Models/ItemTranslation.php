<?php
/**
 * @author Maksim Khodyrev<maximkou@gmail.com>
 * 30.12.16
 */

namespace Nutnet\Artifico2\Navigation\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ItemTranslation
 * @package Nutnet\Artifico2\Navigation\App\Models
 */
class ItemTranslation extends Model
{
    protected $table = 'navigation_trans';

    public $timestamps = false;
}