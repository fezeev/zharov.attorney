<?php

namespace Nutnet\Artifico2\Navigation\App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Kalnoy\Nestedset\NodeTrait;
use Nutnet\Artifico2\Navigation\App\Exceptions\WrongNavigationConfigException;
use Nutnet\Artifico2\Pages\App\Models\Page;

class Item extends Model
{
    use NodeTrait;
    use Translatable;

    protected $table = 'navigation';

    protected $translationForeignKey = 'nav_id';

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;

    public static $STATUSES = [
        self::STATUS_ACTIVE => 'Активен',
        self::STATUS_DISABLED => 'Отключен'
    ];

    const TYPE_LINK = 1;
    const TYPE_PAGE = 2;

    public static $TYPES = [
        self::TYPE_LINK => 'Ссылка',
        self::TYPE_PAGE => 'Страница'
    ];

    /**
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * @var array
     */
    public $translatedAttributes = [
        'name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'value');
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatusName($status)
    {
        return self::$STATUSES[$status];
    }

    /**
     * @param $type
     * @return mixed
     */
    public static function getTypeName($type)
    {
        return self::$TYPES[$type];
    }

    /**
     * @param $value
     * @return array
     */
    public function getLinkAttributesAttribute($value)
    {
        return json_decode($value, true) ?? [];
    }

    /**
     * @param $value
     */
    public function setLinkAttributesAttribute($value)
    {
        $this->attributes['link_attributes'] = json_encode($value);
    }

    /**
     * @return mixed
     * @throws WrongNavigationConfigException
     */
    public function getHref()
    {
        if ($this->type == self::TYPE_LINK) {
            return $this->value;
        }

        $route = config('artifico-navigation.page_route');
        $idParam = config('artifico-navigation.page_route_id_param');
        $idValueField = config('artifico-navigation.page_route_id_value_field');
        $linksAbs = config('artifico-navigation.page_route_is_absolute', true);

        //@todo добавить сообшение об шибке в исключение
        if (!$route || !$idParam) {
            throw new WrongNavigationConfigException(
                '"page_route" and "page_route_id_param" options is required for artifico-navigation package'
            );
        }

        if (!$this->page) {
            // если страница не указана, считаем что страница = страница, на которой рендерится меню
            return '';
        }

        return URL::route($route, [
            $idParam => $this->page->$idValueField
        ], $linksAbs);
    }

    /**
     * @param Builder $query
     * @param Item|int|null $parent
     * @param int $maxDepth
     * @return Builder
     */
    public function scopeActiveChildren(Builder $query, Item $parent = null, $maxDepth = 0)
    {
        $parentDepth = -1;
        if (null !== $parent) {
            $parentDepth = $parent->depth;
        }
        $maxDepth += $parentDepth + 1;

        $query = $query
            ->withDepth()
            ->where('active', '=', 1)
            ->having('depth', '<=', $maxDepth);

        if (null !== $parent) {
            $query->whereDescendantOf($parent->id);
        }

        return $query;
    }

    /**
     * @param array $request
     */
    public function fillFromRequest(array $request)
    {
        $this->name      = $request['name'];
        $this->alias     = $request['alias'] ?: null;
        $this->parent_id = $request['parent_id'];
        $this->type      = $request['type'];
        $this->active    = $request['active'];
        $this->link_attributes = $request['link_attributes'] ?? [];
        $this->noindex    = $request['noindex'];

        switch ($this->type) {
            case Item::TYPE_PAGE:
                $this->value = $request['page_id'];
                break;
            case Item::TYPE_LINK:
                $this->value = $request['url'];
                break;
            default:
                break;
        }

        $translations = [];
        foreach ($this->translatedAttributes as $attr) {
            if (!isset($request[$attr])) {
                continue;
            }

            foreach ($request[$attr] as $locale => $value) {
                if (!isset($translations[$locale])) {
                    $translations[$locale] = [];
                }

                $translations[$locale][$attr] = $value;
            }
        }

        $emptyLocales = array_map(function ($fields) {
            return count(array_filter($fields)) == 0;
        }, $translations);
        $emptyLocales = array_keys(array_filter($emptyLocales));

        if ($this->exists && !empty($emptyLocales)) {
            $this->deleteTranslations($emptyLocales);
        }

        foreach ($translations as $locale => $fields) {
            if (in_array($locale, $emptyLocales)) {
                continue;
            }

            $trans = $this->translateOrNew($locale);
            foreach ($fields as $field => $value) {
                $trans->$field = $value;
            }
        }
    }
}
