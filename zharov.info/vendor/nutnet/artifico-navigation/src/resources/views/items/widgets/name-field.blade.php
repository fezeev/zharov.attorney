<div class="nav-tabs-custom">
    <?php $uniq = 'tab-locales-name'; ?>
    @if (count($locales) > 1)
        <ul class="nav nav-tabs">
            @foreach ($locales as $code => $name)
                <li @if($code == $activeLocale)class="active"@endif>
                    <a href="#tab_{{ $uniq }}_{{ $code }}" data-toggle="tab">{{ $name }}</a>
                </li>
            @endforeach
        </ul>
    @endif

    <div class="tab-content">
        @foreach ($locales as $code => $name)
            <div class="tab-pane{{ $code == $activeLocale ? ' active': '' }}" id="tab_{{ $uniq }}_{{ $code }}">
                <div class="form-group {{ $errors->has("name.$code") ? 'has-error' : '' }}">
                    <label for="name_{{ $code }}">Название</label>
                    <input type="text" class="form-control" id="name_{{ $code }}" name="name[{{ $code }}]"
                           value="{{ old("name.$code", isset($item) ? $item->translateOrNew($code)->name : '') }}">
                    @if ($errors->has("name.$code"))
                        {!! $errors->first("name.$code",'<span class="help-block">:message</span>')  !!}
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>