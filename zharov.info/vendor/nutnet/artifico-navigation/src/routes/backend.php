<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => config('artifico2.root_path'), 'middleware' => ['backend'], 'as' => 'admin.'], function () {

        Route::resource('navigation', 'Nutnet\Artifico2\Navigation\App\Http\Controllers\Items\Items');
        Route::match(['get', 'post'], 'navigation/sort', 'Nutnet\Artifico2\Navigation\App\Http\Controllers\Items\Items@sort')->name('navigation.sort');
    });
});
