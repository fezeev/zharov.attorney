-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql.zharov.nutnetdev.ru
-- Generation Time: Oct 16, 2017 at 05:35 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `_dev_zharov`
--

-- --------------------------------------------------------

--
-- Table structure for table `extra_fields`
--

CREATE TABLE `extra_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `origin_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `entity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `icon`, `href`, `params`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Система', 'fa ion-android-settings', '#', '', NULL, '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(2, 'Настройки', 'fa fa-circle-o', 'admin.settings.index', '', 1, '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(3, 'Пользователи', 'fa fa-circle-o', 'admin.administrators.index', '', 1, '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(4, 'Права', 'fa fa-circle-o', 'admin.usergroups.index', '', 1, '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(5, 'Страницы', 'fa fa-newspaper-o', 'admin.pages.index', '', NULL, '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(6, 'Навигация', 'fa fa-sitemap', 'admin.navigation.index', '', NULL, '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(7, 'Публикации', 'fa fa-sticky-note', 'admin.publications.index', '', NULL, '2017-08-30 20:02:12', '2017-08-30 20:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(37, '2014_10_12_000000_create_users_table', 1),
(38, '2014_10_12_100000_create_password_resets_table', 1),
(39, '2017_05_16_092452_create_permission_tables', 1),
(40, '2017_05_16_092552_update_roles_table', 1),
(41, '2017_05_16_092652_update_permissions_table', 1),
(42, '2017_05_16_092752_insert_permissions_table', 1),
(43, '2017_05_16_092852_insert_default_administrator_and_roles', 1),
(44, '2017_05_16_092952_insert_default_settings_permissions', 1),
(45, '2017_05_16_093052_create_settings_table', 1),
(46, '2017_05_16_093152_insert_default_settings', 1),
(47, '2017_05_16_093252_create_menus_table', 1),
(48, '2017_05_16_093352_insert_index_permissions', 1),
(49, '2017_05_16_093452_make_settings_translatable', 1),
(50, '2017_05_16_093552_extra_fields_table', 1),
(51, '2017_05_16_132822_create_pages_table', 1),
(52, '2017_05_16_132922_insert_pages_module_menu_table', 1),
(53, '2017_05_16_133022_insert_default_pages_permissions', 1),
(54, '2017_05_16_133122_add_meta_and_make_translatable', 1),
(55, '2017_05_16_161205_create_navigation_table', 1),
(56, '2017_05_16_161305_insert_navigation_menu_table', 1),
(57, '2017_05_16_161405_insert_default_navigation_permissions', 1),
(58, '2017_05_16_161505_update_navigation_table', 1),
(59, '2017_05_16_161605_make_navigation_translatable', 1),
(60, '2017_05_16_161705_navigation_add_link_attributes', 1),
(61, '2017_05_16_161805_make_alias_optional', 1),
(62, '2017_05_19_150841_fix_settings_translatable', 1),
(63, '2017_08_14_113900_create_main_nav_parent', 1),
(64, '2017_08_14_131200_insert_about_page', 1),
(65, '2017_08_14_140800_insert_default_publications_permissions', 1),
(66, '2017_08_14_152400_insert_publications_module_menu_table', 1),
(67, '2017_08_14_163300_Insert_practices_page', 1),
(68, '2017_08_15_115500_Insert_Publication_Page_Nav', 1),
(69, '2017_08_15_121833_create_publications_table', 1),
(70, '2017_08_16_142300_insert_contacts_page', 1),
(71, '2017_08_22_131300_Insert_receiver_email', 1),
(72, '2017_08_28_103900_insert_index_page', 1),
(73, '2017_09_18_134900_insert_zharov_phone', 2),
(74, '2017_09_20_173916_add_email_logs_table', 3),
(75, '2017_09_20_173916_change_user_permission_names', 3),
(76, '2017_09_19_155800_insert_email_text', 4),
(77, '2017_09_21_130100_add_publications_date_collumn', 4),
(78, '2017_09_22_111400_insert_analytics_scripts_into_settings', 5),
(79, '2017_09_26_113900_insert_robotstxt_settings', 6),
(80, '2017_09_19_120859_add_email_logs_table', 3),
(81, '2017_09_14_165008_change_user_permission_names', 3),
(82, '2017_10_09_162400_insert_appointment_href_setting', 7),
(83, '2017_10_09_173800_delete_email_text', 7),
(84, '2017_10_09_174400_delete_email_logs_table', 7),
(85, '2017_10_09_174600_delete_receiver_email', 7);

-- --------------------------------------------------------

--
-- Table structure for table `navigation`
--

CREATE TABLE `navigation` (
  `id` int(10) UNSIGNED NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `link_attributes` text COLLATE utf8_unicode_ci,
  `noindex` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `navigation`
--

INSERT INTO `navigation` (`id`, `alias`, `value`, `type`, `active`, `created_at`, `updated_at`, `_lft`, `_rgt`, `parent_id`, `link_attributes`, `noindex`) VALUES
(1, 'main_menu', 'main_menu', '1', '1', NULL, NULL, 1, 11, NULL, '[]', 0),
(2, 'about', '1', '2', '1', NULL, '2017-08-31 18:57:54', 2, 3, 1, '[]', 0),
(3, 'practices', '2', '2', '1', NULL, '2017-08-31 17:21:12', 4, 5, 1, '[]', 0),
(4, 'publications', 'publications', '1', '1', NULL, NULL, 6, 7, 1, '[]', 0),
(5, 'contacts', '3', '2', '1', NULL, NULL, 8, 9, 1, '[]', 0),
(6, 'index', '', '2', '0', NULL, '2017-09-15 23:06:17', 9, 10, 1, '[]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `navigation_trans`
--

CREATE TABLE `navigation_trans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nav_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `navigation_trans`
--

INSERT INTO `navigation_trans` (`id`, `nav_id`, `name`, `locale`) VALUES
(1, 1, 'Главное меню', 'ru'),
(2, 2, 'О Жарове', 'ru'),
(3, 3, 'Практики', 'ru'),
(4, 4, 'Публикации', 'ru'),
(5, 5, 'Контакты', 'ru'),
(6, 6, 'Главная страница', 'ru');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `alias`, `active`, `created_at`, `updated_at`) VALUES
(1, 'about', '1', NULL, '2017-09-01 16:36:52'),
(2, 'practices', '1', NULL, '2017-08-31 17:21:05'),
(3, 'contacts', '1', NULL, NULL),
(4, 'index', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages_trans`
--

CREATE TABLE `pages_trans` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_other` text COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages_trans`
--

INSERT INTO `pages_trans` (`id`, `page_id`, `name`, `content`, `meta_name`, `meta_description`, `meta_keywords`, `meta_other`, `locale`) VALUES
(1, 1, 'О Жарове', '<main class=\"content\">\r\n<div class=\"content__wrapper\">\r\n<h1 class=\"content__title\">О Жарове</h1>\r\n\r\n<div class=\"content__block\">\r\n<h2 class=\"content__block-title\">Специализация</h2>\r\n\r\n<p class=\"content__text\">С&nbsp;2007-го&nbsp;Антон Алексеевич Жаров специализируется исключительно на&nbsp;семейном и&nbsp;ювенальном праве, защите прав детей, в&nbsp;том числе&nbsp;оставшихся без попечения родителей.</p>\r\n\r\n<p class=\"content__text\">Урегулирует семейные споры (в&nbsp;том числе&nbsp;трансграничные), включая вопросы раздела имущества, лишения родительских прав, незаконного перемещения ребёнка через границу одним из&nbsp;супругов, места жительства ребёнка и&nbsp;порядка общения с&nbsp;ним.</p>\r\n\r\n<p class=\"content__sub-text\">Ведёт дела об&nbsp;усыновлении (в&nbsp;том числе и&nbsp;международном), об&nbsp;опеке и&nbsp;попечительстве над несовершеннолетними.</p>\r\n</div>\r\n\r\n<div class=\"content__block\">\r\n<h2 class=\"content__block-title\">Деятельность</h2>\r\n\r\n<p class=\"content__text\">В&nbsp;2007-2010 годах&nbsp;&mdash; эксперт рабочих групп обеих палат Федерального Собрания Российской Федерации, а&nbsp;также министерств и&nbsp;ведомств&nbsp;РФ при создании законодательства о&nbsp;защите прав детей, опеке, семейного законодательства.</p>\r\n\r\n<p class=\"content__text\">Адвокат палаты города Москвы, руководитель &laquo;<a href=\"http://azh.ru/\" target=\"_blank\" title=\"Команда адвоката Жарова\">Команды адвоката Жарова</a>&raquo;.</p>\r\n\r\n<p class=\"content__sub-text\">Практикует в&nbsp;российских судах с&nbsp;2000&nbsp;года.</p>\r\n</div>\r\n\r\n<div class=\"content__block\">\r\n<h2 class=\"content__block-title\">Достижения</h2>\r\n\r\n<p class=\"content__text\">Основатель и&nbsp;глава &laquo;Команды адвоката Жарова&raquo;&nbsp;&mdash; единственной в&nbsp;России специализированной юридической фирмы, практикующей в&nbsp;сфере семейного и&nbsp;ювенального (детского) права, в&nbsp;том числе&nbsp;занимающейся трансграничными спорами родителей и&nbsp;международным усыновлением.</p>\r\n\r\n<p class=\"content__text\">Является одним из&nbsp;разработчиков закона &laquo;Об&nbsp;опеке и&nbsp;попечительстве&raquo;, ряда изменений в&nbsp;Семейный кодекс&nbsp;РФ, постановлений Правительства РФ&nbsp;по&nbsp;вопросам установления опеки над несовершеннолетними, усыновления.</p>\r\n\r\n<p class=\"content__sub-text\">Участвовал в&nbsp;создании программы подготовки будущих усыновителей в&nbsp;РФ.</p>\r\n\r\n<p class=\"content__text\">Выступал в&nbsp;качестве неправительственного эксперта при разработке нормативных актов по&nbsp;вопросам подготовки усыновителей и&nbsp;опеки над детьми в&nbsp;Литовской Республике.</p>\r\n\r\n<p class=\"content__text\">В&nbsp;2016 году создал и&nbsp;возглавил (в&nbsp;качестве научного директора) <a href=\"http://isppp.site/\" target=\"_blank\" title=\"ИСППП\">Институт семейных просветительских и&nbsp;правовых программ</a> (ИСППП), цель которого&nbsp;&mdash; разработка, популяризация и&nbsp;внедрение современных методов защиты прав семьи и&nbsp;детей.</p>\r\n\r\n<p class=\"content__text\">Автор более 20&nbsp;научных и&nbsp;научно-популярных работ по&nbsp;вопросам лишения родительских прав и&nbsp;сложным случаям усыновления, а&nbsp;также свыше 200 статей по&nbsp;проблемам семейного и&nbsp;ювенального права.</p>\r\n\r\n<p class=\"content__text\">Работает над кандидатской диссертацией.</p>\r\n</div>\r\n\r\n<div class=\"content__link-wrapper\"><a class=\"content__link\" href=\"/practices\">Подробнее о практиках</a></div>\r\n</div>\r\n</main>\r\n', 'О Жарове – Адвокат Жаров', 'Профессиональный путь и достижения адвоката Жарова.', '', '', 'ru'),
(2, 2, 'Практики', '<main class=\"content\">\r\n<div class=\"content__wrapper\">\r\n<h1 class=\"content__title\">Практики</h1>\r\n\r\n<div class=\"content__block content__block--practice\">\r\n<h2 class=\"content__block-title\">Брачно-семейная</h2>\r\n\r\n<ul class=\"content__list\">\r\n	<li class=\"content__text content__text--practice\">Расторжение брака</li>\r\n	<li class=\"content__text content__text--practice\">Раздел имущества</li>\r\n	<li class=\"content__text content__text--practice\">Трансграничные семейные споры</li>\r\n	<li class=\"content__text content__text--practice\">Наследственные споры и&nbsp;защита имущества</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"content__block content__block--practice\">\r\n<h2 class=\"content__block-title\">Защита прав несовершеннолетних</h2>\r\n\r\n<ul class=\"content__list\">\r\n	<li class=\"content__text content__text--practice\">Место жительства ребёнка с&nbsp;одним из&nbsp;родителей</li>\r\n	<li class=\"content__text content__text--practice\">Порядок общения ребёнка с&nbsp;отдельно проживающим родителем</li>\r\n	<li class=\"content__text content__text--practice\">Возврат ребёнка из-за границы</li>\r\n	<li class=\"content__text content__text--practice\">Лишение родительских прав</li>\r\n	<li class=\"content__text content__text--practice\">Защита личных неимушественных прав ребёнка (на&nbsp;имя, фамилию и&nbsp;пр.)</li>\r\n	<li class=\"content__text content__text--practice\">Международная защита прав детей</li>\r\n	<li class=\"content__text content__text--practice\">Алименты на&nbsp;детей</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"content__block content__block--practice\">\r\n<h2 class=\"content__block-title\">Ювенально-уголовная</h2>\r\n\r\n<ul class=\"content__list\">\r\n	<li class=\"content__text content__text--practice\">Защита несовершеннолетних правонарушителей</li>\r\n	<li class=\"content__text content__text--practice\">Представительство интресов несовершеннолетних потерпевших</li>\r\n	<li class=\"content__text content__text--practice\">Защита прав несовершеннолетних в&nbsp;комиссиях по&nbsp;делам несовершеннолетних</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"content__block content__block--practice\">\r\n<h2 class=\"content__block-title\">Опека и&nbsp;попечительство</h2>\r\n\r\n<ul class=\"content__list\">\r\n	<li class=\"content__text content__text--practice\">Усыновление</li>\r\n	<li class=\"content__text content__text--practice\">Опека и&nbsp;попечительство над детьми</li>\r\n	<li class=\"content__text content__text--practice\">Установление недееспособности</li>\r\n	<li class=\"content__text content__text--practice\">Опека над недееспособными</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"content__link-wrapper\"><a class=\"content__link content__link--record\" href=\"http://azh.ru/ru/kontakty/zapisatsya-na-priem/\" target=\"_blank\" >Записаться на&nbsp;прием</a></div>\r\n</div>\r\n</main>\r\n', 'Практики – Адвокат Жаров', 'Список практик адвоката Жарова: брачно-семейная, защита прав несовершеннолетних, ювенально-уголовная, опека и попечительство.', '', '', 'ru'),
(3, 3, 'Контакты', '<main class=\"contacts\">\r\n<div class=\"contacts__wrapper\">\r\n<h1 class=\"contacts__title\">Контакты</h1>\r\n\r\n<div class=\"contacts__block\">\r\n<p class=\"contacts__block-title\">Телефон</p>\r\n<a class=\"contacts__information contacts__information--phone\" href=\"tel:+74952270121\">+7 (495) 227-01-21</a><a class=\"contacts__more-info\" href=\"http://azh.ru/ru/kontakty/adres-ofisa/kontakty-dlya-zvonkov-iz-za-rubezha-i-iz-drugix-gorodov-rossii/\" target=\"_blank\">В других городах</a></div>\r\n\r\n<div class=\"contacts__block\">\r\n<p class=\"contacts__block-title\">Почта</p>\r\n<a class=\"contacts__information contacts__information--email\" href=\"mailto:anton@zharov.info\" target=\"_blank\">anton@zharov.info</a></div>\r\n\r\n<div class=\"contacts__block\">\r\n<p class=\"contacts__block-title\">Адрес</p>\r\n<a class=\"contacts__information contacts__information--address\" href=\"https://yandex.ru/maps/213/moscow/?ll=37.613655%2C55.766770&z=16&mode=search&text=%D0%9F%D0%B5%D1%82%D1%80%D0%BE%D0%B2%D1%81%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%B5%D1%80%D0%B5%D1%83%D0%BB%D0%BE%D0%BA%2C%20%D0%B4.5%2C%20%D1%81%D1%82%D1%80.%205%2C%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0%2C%20107031&sll=53.211463%2C56.854468&sspn=0.256462%2C0.064047&ol=biz&oid=1244445164\" target=\"_blank\">Петровский переулок, д.&nbsp;5, стр.&nbsp;5, Москва, 107031</a></div>\r\n</div>\r\n</main>\r\n', 'Контакты – Адвокат Жаров', 'Контактная информация: телефон, электронная почта, адрес офиса и как к нему добраться.', '', '', 'ru'),
(4, 4, 'Главная страница', '<main class=\"index-content\">\r\n    <div class=\"index-content__wrapper\">\r\n        <div class=\"index-content__descr\">\r\n            <h1 class=\"index-content__name\">Адвокат Жаров</h1>\r\n            <p class=\"index-content__work\">Семейное и детское право</p>\r\n            <p class=\"index-content__work-descr\">Занимаюсь сложными случаями усыновления. Лично веду каждое дело — от начала до конца. Использую правовые нюансы, которые не замечают другие.</p>\r\n            <div class=\"index-content__more-info-block\"><a href=\"/about\" class=\"index-content__more-info\">Подробнее</a></div>\r\n        </div>\r\n        <a class=\"index-content__photo-block\" href=\"/about\"><img src=\"images/zharov-photo@1x.png\" srcset=\"images/zharov-photo@2x.png 2x\" alt=\"Адвокат Жаров\" class=\"index-content__image\"></a>\r\n    </div>\r\n</main>\r\n', 'Адвокат Жаров – семейное и детское право', 'Занимаюсь сложными случаями усыновления. Лично веду каждое дело — от начала до конца. Использую правовые нюансы, которые не замечают другие.', '', '', 'ru');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `human_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `group`, `human_name`, `description`, `created_at`, `updated_at`) VALUES
(1, '*', 'CMS', 'Суперпользователь', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(2, 'visit backend', 'CMS', 'Посещение панели управления', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(3, 'view administrators', 'Пользователи', 'Просмотр пользователей', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(4, 'create administrator', 'Пользователи', 'Создание пользователей', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(5, 'edit administrator', 'Пользователи', 'Редактирование пользователей', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(6, 'delete administrator', 'Пользователи', 'Удаление пользователей', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(7, 'view roles', 'Группы', 'Просмотр ролей', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(8, 'create role', 'Группы', 'Создание роли', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(9, 'edit role', 'Группы', 'Редактирование роли', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(10, 'delete role', 'Группы', 'Удаление роли', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(11, 'view settings', 'Настройки', 'Просмотр настроек', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(12, 'create setting', 'Настройки', 'Создание настройки', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(13, 'edit setting', 'Настройки', 'Редактирование настройки', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(14, 'delete setting', 'Настройки', 'Удаление настройки', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(15, 'view index', 'CMS', 'Посещение главной страницы', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(16, 'view pages', 'Страницы', 'Просмотр страниц', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(17, 'create page', 'Страницы', 'Создание страницы', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(18, 'edit page', 'Страницы', 'Редактирование страницы', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(19, 'delete page', 'Страницы', 'Удаление страницы', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(20, 'view navitems', 'Навигация', 'Просмотр структуры', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(21, 'create navitem', 'Навигация', 'Создание пункта', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(22, 'edit navitem', 'Навигация', 'Редактирование пункта', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(23, 'delete navitem', 'Навигация', 'Удаление пункта', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11'),
(24, 'view publications', 'Публикации', 'Просмотр публикаций', '', '2017-08-30 20:02:12', '2017-08-30 20:02:12'),
(25, 'create publications', 'Публикации', 'Создание публикации', '', '2017-08-30 20:02:12', '2017-08-30 20:02:12'),
(26, 'edit publications', 'Публикации', 'Редактирование публикации', '', '2017-08-30 20:02:12', '2017-08-30 20:02:12'),
(27, 'delete publications', 'Публикации', 'Удаление публикации', '', '2017-08-30 20:02:12', '2017-08-30 20:02:12');

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--

CREATE TABLE `publications` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `publications`
--

INSERT INTO `publications` (`id`, `created_at`, `updated_at`, `author`, `title`, `href`, `categories_id`, `date`) VALUES
(1, '2017-08-30 20:07:38', '2017-09-29 17:36:38', 'Москва 24', 'Как воспитывает свою дочь семья слепых в Красногорске', 'http://tv.m24.ru/videos/137923', 1, '2017-09-06'),
(3, '2017-08-30 20:08:31', '2017-09-29 17:37:09', 'Лента.Ру', 'Мучение, а не жизнь', 'https://lenta.ru/articles/2017/09/13/omerzitelno/', 1, '2017-09-13'),
(4, '2017-08-30 22:58:19', '2017-09-28 22:52:11', 'Настоящее Время', 'Неделя с Тимуром Олевским. 10 июля 2016', 'https://www.currenttime.tv/a/27847770.html', 1, '2016-07-10'),
(5, '2017-08-30 22:58:26', '2017-09-28 22:54:27', 'Православие и мир', 'Дети, сколько у вас дома телевизоров и произведений искусства?', 'http://www.pravmir.ru/deti-skolko-u-vas-doma-televizorov-i-proizvedeniy-iskusstva/', 3, '2016-05-18'),
(6, '2017-08-30 22:58:37', '2017-09-28 23:50:26', 'КоммерсантЪ', 'Москва поделила сирот на своих и чужих', 'http://www.kommersant.ru/doc/2927938', 3, '2016-03-02'),
(8, '2017-08-30 22:59:02', '2017-09-28 23:50:52', 'Эхо Москвы', 'Вон из семьи', 'http://echo.msk.ru/blog/mediazona/1616092-echo/', 2, '2015-09-04'),
(9, '2017-08-31 17:06:08', '2017-09-28 23:51:30', 'Вести FM', 'Юридическая консультация. Права детей', 'http://radiovesti.ru/brand/61001/episode/1367242/', 2, '2015-02-01'),
(12, '2017-09-14 00:41:37', '2017-09-28 23:52:34', 'Дождь', 'Госдума запретит иностранное усыновление', 'https://tvrain.ru/teleshow/dzjadko_vechernee_shou/ostanetsja_tolko_italija_gosduma_zapretit_inostrannoe_usynovlenie-376280/', 1, '2014-10-06'),
(13, '2017-09-14 00:41:44', '2017-09-28 23:53:22', 'Маяк', 'Разговор с Антоном Жаровым', 'http://radiomayak.ru/shows/episode/id/1133734/', 2, '2014-08-21'),
(14, '2017-09-14 00:41:57', '2017-09-28 23:53:44', 'Измени одну жизнь', 'Антон Жарова об усыновлении', 'http://changeonelife.ru/videos/lyudi-kak-lyudi-advokat-anton-zharov/', 1, '2014-07-09'),
(33, '2017-09-21 01:38:19', '2017-09-28 23:54:48', 'Эхо Москвы', 'Дело Максима Кузьмина', 'http://echo.msk.ru/programs/razvorot/1041076-echo', 2, '2013-03-29'),
(34, '2017-09-21 01:51:22', '2017-09-28 23:55:44', 'Эхо Москвы', 'О деле 4-летнего Глеба Агеева', 'http://echo.msk.ru/programs/razvorot-morning/582109-echo', 2, '2009-03-30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `human_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `human_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'administrators', 'Администраторы', '', '2017-08-30 20:02:10', '2017-08-30 20:02:10'),
(2, 'editors', 'Редакторы', '', '2017-08-30 20:02:11', '2017-08-30 20:02:11');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `key` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`key`, `group`, `name`) VALUES
('cms.analytics', 'CMS', 'Скрипты для аналитик'),
('cms.appointmenthref', 'CMS', 'Ссылка для записи на прием'),
('cms.phone', 'CMS', 'Телефон для связи'),
('cms.robots', 'CMS', 'Файл robots.txt'),
('cms.shortsitename', 'CMS', 'Короткое название системы управления'),
('cms.sitename', 'CMS', 'Название системы управления');

-- --------------------------------------------------------

--
-- Table structure for table `settings_trans`
--

CREATE TABLE `settings_trans` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_key` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings_trans`
--

INSERT INTO `settings_trans` (`id`, `setting_key`, `value`, `locale`) VALUES
(1, 'cms.shortsitename', 'ZH', 'ru'),
(2, 'cms.sitename', 'Zharov.info', 'ru'),
(10, 'cms.phone', '+7 (495) 227-01-21', 'ru'),
(12, 'cms.analytics', '<script>\r\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');\r\n\r\n  ga(\'create\', \'UA-106851333-1\', \'auto\');\r\n  ga(\'send\', \'pageview\');\r\n\r\n</script>\r\n\r\n<script type=\"text/javascript\" >\r\n    (function (d, w, c) {\r\n        (w[c] = w[c] || []).push(function() {\r\n            try {\r\n                w.yaCounter46224657 = new Ya.Metrika({\r\n                    id:46224657,\r\n                    clickmap:true,\r\n                    trackLinks:true,\r\n                    accurateTrackBounce:true\r\n                });\r\n            } catch(e) { }\r\n        });\r\n\r\n        var n = d.getElementsByTagName(\"script\")[0],\r\n            s = d.createElement(\"script\"),\r\n            f = function () { n.parentNode.insertBefore(s, n); };\r\n        s.type = \"text/javascript\";\r\n        s.async = true;\r\n        s.src = \"https://mc.yandex.ru/metrika/watch.js\";\r\n\r\n        if (w.opera == \"[object Opera]\") {\r\n            d.addEventListener(\"DOMContentLoaded\", f, false);\r\n        } else { f(); }\r\n    })(document, window, \"yandex_metrika_callbacks\");\r\n</script>\r\n<noscript><div><img src=\"https://mc.yandex.ru/watch/46224657\" style=\"position:absolute; left:-9999px;\" alt=\"\" /></div></noscript>', 'ru'),
(13, 'cms.robots', 'User-agent: *\r\nDisallow: /css/\r\nDisallow: /img/\r\nDisallow: /js/\r\nHost: zharov.info', 'ru'),
(14, 'cms.appointmenthref', 'http://azh.ru/ru/kontakty/zapisatsya-na-priem/', 'ru');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nutnet-support', 'support@nutnet.ru', '$2y$10$.9ROG2UQk1YS2GN8bcG9d.Xr8pnhc7JkY8k227L5cMmCaXKTsmzri', '6acfyyNrtrsrcPyNXETOwpg310hz9ueWnxulQtGI1KsxZJc2c4T4sIjd3WsH', '2017-08-30 20:02:11', '2017-09-27 01:00:46'),
(7, 'Антон Жаров', 'anton@zharov.info', '$2y$10$3n5XduXLna4f2AqvaTGAtuMlvsq0VJUofix5f30u4iNVq9hzW7KKa', NULL, '2017-09-28 18:52:11', '2017-09-28 18:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_has_permissions`
--

CREATE TABLE `user_has_permissions` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_roles`
--

CREATE TABLE `user_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_has_roles`
--

INSERT INTO `user_has_roles` (`role_id`, `user_id`) VALUES
(1, 1),
(1, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `extra_fields`
--
ALTER TABLE `extra_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `extra_fields_identifier_entity_entity_id_unique` (`identifier`,`entity`,`entity_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navigation`
--
ALTER TABLE `navigation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `navigation_alias_unique` (`alias`),
  ADD KEY `navigation__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `navigation_trans`
--
ALTER TABLE `navigation_trans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `navigation_trans_nav_id_locale_unique` (`nav_id`,`locale`),
  ADD KEY `navigation_trans_locale_index` (`locale`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_alias_unique` (`alias`);

--
-- Indexes for table `pages_trans`
--
ALTER TABLE `pages_trans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_trans_page_id_locale_unique` (`page_id`,`locale`),
  ADD KEY `pages_trans_locale_index` (`locale`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`),
  ADD UNIQUE KEY `permissions_human_name_unique` (`human_name`);

--
-- Indexes for table `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD UNIQUE KEY `roles_human_name_unique` (`human_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `settings_trans`
--
ALTER TABLE `settings_trans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_trans_setting_key_locale_unique` (`setting_key`,`locale`),
  ADD KEY `settings_trans_locale_index` (`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_has_permissions`
--
ALTER TABLE `user_has_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `user_has_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `user_has_roles`
--
ALTER TABLE `user_has_roles`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `user_has_roles_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `extra_fields`
--
ALTER TABLE `extra_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `navigation`
--
ALTER TABLE `navigation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `navigation_trans`
--
ALTER TABLE `navigation_trans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pages_trans`
--
ALTER TABLE `pages_trans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `publications`
--
ALTER TABLE `publications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `settings_trans`
--
ALTER TABLE `settings_trans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `navigation_trans`
--
ALTER TABLE `navigation_trans`
  ADD CONSTRAINT `navigation_trans_nav_id_foreign` FOREIGN KEY (`nav_id`) REFERENCES `navigation` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages_trans`
--
ALTER TABLE `pages_trans`
  ADD CONSTRAINT `pages_trans_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `settings_trans`
--
ALTER TABLE `settings_trans`
  ADD CONSTRAINT `settings_trans_setting_key_foreign` FOREIGN KEY (`setting_key`) REFERENCES `settings` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_has_permissions`
--
ALTER TABLE `user_has_permissions`
  ADD CONSTRAINT `user_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_has_roles`
--
ALTER TABLE `user_has_roles`
  ADD CONSTRAINT `user_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
